open import lib

module Tree where

data Tree : Set where
  Leaf : Tree
  Node : ℕ → Tree → Tree → Tree

