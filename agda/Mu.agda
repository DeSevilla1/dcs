open import lib

open import FunctorCodes

module Mu{n : ℕ}(F : Fnc n) where

{-# NO_UNIVERSE_CHECK #-}
data Mu : Set where
  inMu : ∀{R : Set} → (r : R → Mu) → (d : interp F R) → Mu

