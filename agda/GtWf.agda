open import lib
open import FunctorCodes

module GtWf{n : ℕ}(F : Fnc n) where
open import Mu
open import Eq
open import Gt
open import GtTrans

↓GtEq : ∀{n : ℕ}{F : Fnc n}{x y : Mu F} → ↓ (Gt F) x → Eq F x y → ↓ (Gt F) y
↓GtEq{n}{F}{x}{y} (pf↓ f) e = pf↓ λ{y'} u → f (EqGtTrans F e u)

↓GtShift : ∀{R : Set}{r : R → Mu F}{d : R} → 
            ↓ (Gt F) (r d) →
            ↓ (λ x x' → Gt F (r x) (r x')) d 
↓GtShift{R}{r} (pf↓ q) = pf↓ λ {y} g → ↓GtShift (q g) 

↓GtCongSum1 : ∀{R : Set}{r : R → Mu F}
              {n1 : ℕ}{F1 : Fnc n1}{d : interp F1 R} 
              {n2 : ℕ}{F2 : Fnc n2} →
              ↓ (interpCong F1 (λ v v' → Gt F (r v) (r v'))) d → 
              ↓ (interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v'))) (inj₁ d)
↓GtCongSum1{R}{r}{n1}{F1}{d}{n2}{F2} (pf↓ ic) = pf↓ h
  where h : ∀{y : interp (Sum F1 F2) R} → 
             interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v')) (inj₁ d) y →
             ↓ (interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v'))) y
        h {inj₁ d'} q = ↓GtCongSum1 (ic q)

↓GtCongSum2 : ∀{R : Set}{r : R → Mu F}
              {n1 : ℕ}{F1 : Fnc n1} 
              {n2 : ℕ}{F2 : Fnc n2}{d : interp F2 R} →
              ↓ (interpCong F2 (λ v v' → Gt F (r v) (r v'))) d → 
              ↓ (interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v'))) (inj₂ d)
↓GtCongSum2{R}{r}{n1}{F1}{n2}{F2}{d} (pf↓ ic) = pf↓ h
  where h : ∀{y : interp (Sum F1 F2) R} → 
             interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v')) (inj₂ d) y →
             ↓ (interpCong (Sum F1 F2) (λ v v' → Gt F (r v) (r v'))) y
        h {inj₂ d'} q = ↓GtCongSum2 (ic q)

mutual
 GtWf : ∀{x : Mu F} → ↓ (Gt F) x
 GtWf{x} = pf↓ h
  where
   mutual
    h : ∀{x y : Mu F} → Gt F x y → ↓ (Gt F) y
    h{x}{y} (gt1{R} r d _ U revealU dec) = gt1h{n}{F} d dec
      where
        mutual
          gt1h : {n' : ℕ}{F' : Fnc n'} → 
                 (d : interp F' R) →
                 {y : Mu F} → 
                 gtFMu F r F' U d y → 
                 ↓ (Gt F) y
          gt1h {F' = Sum{n'}{n''} F' F''} (inj₁ d) dec = gt1h2 d dec 
          gt1h {F' = Sum{n'}{n''} F' F''} (inj₂ d) dec = gt1h2 d dec 
          gt1h {F' = Prod F' F''} (d , d') (inj₁ dec) = gt1h2 d dec
          gt1h {F' = Prod F' F''} (d , d') (inj₂ dec) = gt1h2 d' dec
          gt1h {F' = Arrow X F'} d (x , dec) = gt1h2 (d x) dec
          gt1h {F' = Param} d {y} dec = ↓> (GtWf{r d}) (revealU dec)
           
          gt1h2 : {n' : ℕ}{F' : Fnc n'} → 
                  (d : interp F' R) →
                  {y : Mu F} → 
                  geqFMu F r F' U d y → 
                  ↓ (Gt F) y
          gt1h2 {F' = Param} d (inj₁ eq) = ↓GtEq (GtWf{r d}) eq
          gt1h2{n'}{F'} d (inj₂ dec) = gt1h{n'}{F'} d dec
         
    h (gtCong{R} r d d' U revealU ic rch) = {!!}

 CongGtWf : ∀{R : Set}{r : R → Mu F}{d : interp F R} → reachesParam F d →
             ↓ (interpCong F (λ v v' → Gt F (r v) (r v'))) d
 CongGtWf{R}{r}{d} rch = pf↓ (h{n}{F} rch)
   where
     h : {n' : ℕ}{F' : Fnc n'}
         {d : interp F' R} →
         reachesParam F' d → 
         {d' : interp F' R} →
         interpCong F' (λ v v' → Gt F (r v) (r v')) d d' → 
         ↓ (interpCong F' (λ v v' → Gt F (r v) (r v'))) d'
     h {F' = Sum F' F''} {inj₁ d} rch {inj₁ d'} ic = ↓GtCongSum1 (h{F' = F'} rch ic) 
     h {F' = Sum F' F''} {inj₂ d} rch {inj₂ d'} ic = ↓GtCongSum2 (h{F' = F''} rch ic) 
     h {F' = Prod F' F''} {d1 , d1'} (inj₁ rch) {d2 , d2'} (ic , ic') = {!!}
     h {F' = Prod F' F''} {d1 , d1'} (inj₂ rch) {d2 , d2'} (ic , ic') = {!!}

     h {n'} {Arrow X F'} {d} rch {d'} ic = {!!}
     h {n'} {Param} {d} rch {d'} ic =  ↓GtShift (↓> (GtWf{r d}) ic)

{-
      j {-(dig{n}{F} d d' ic)-} ? (reachesParamCong{n}{F}{R} d d' {λ v v' → U (r v) (r v')} ic rch)
      where
        dig : ∀{n' : ℕ}{F' : Fnc n'} → 
               (d1 : interp F' R) →
               (d2 : interp F' R) →
               interpCong F' (λ v v' → U (r v) (r v')) d1 d2 → 
               interpPred F' (λ v → ↓ (Gt F) (r v)) d2
        dig {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) ic = dig{F' = F'} d1 d2 ic
        dig {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) ic = dig{F' = F''} d1 d2 ic
        dig {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') =
          (dig{F' = F'} d1 d2 ic , dig{F' = F''} d1' d2' ic')
        dig {F' = Arrow X F'} d1 d2 ic {x} = dig{F' = F'} (d1 x) (d2 x) (ic x)
        dig {F' = Param} d1 d2 ic = h (revealU ic)
        dig {F' = Other X} d1 d2 ic = triv

        j : ∀{n' : ℕ}{F' : Fnc n'} → 
             {d : interp F' R} →
             ↓ (interpCong F' (λ v v' → U (r v) (r v'))) d →
             ↓ (Gt F') (inMu ? d)
        j ict = pf↓ (k ?)
          where
           k : ∀{n' : ℕ}{F' : Fnc n'} → 
                {d : interp F' R} →
                ↓ (interpCong F' (λ v v' → U v v')) ? →
                ∀{y : Mu F'} →
                Gt F' (inMu ? d) y →
                ↓ (Gt F') y
           k = ?
-}           {-
           k{n'}{F'} ip rch (gt1 r2 d2 y U revealU dec) = ? -- kh{n'}{F'} d2 ip dec 
{-             where
              mutual
               kh : ∀{n'' : ℕ}{F'' : Fnc n''} → 
                     {R : Set}{r : R → Mu F'} → 
                     (d : interp F'' R) →
                     interpPred F'' (λ v → ↓ (Gt F') (r v)) d →
                     gtFMu F' r F'' U d y → 
                     ↓ (Gt F') y
               kh {F'' = Sum F'' F'''} (inj₁ d) ip g = khe d ip g
               kh {F'' = Sum F'' F'''} (inj₂ d) ip g = khe d ip g
               kh {F'' = Prod F'' F'''} (d , d') (ip , ip') (inj₁ g) = khe d ip g
               kh {F'' = Prod F'' F'''} (d , d') (ip , ip') (inj₂ g) = khe d' ip' g
               kh {F'' = Arrow X F''} d ip (x , g) = khe (d x) ip g
               kh {F'' = Param} d ip g = ↓> ip (revealU g)
               khe : ∀{n'' : ℕ}{F'' : Fnc n''} → 
                      {R : Set}{r : R → Mu F'} → 
                      (d : interp F'' R) →
                      interpPred F'' (λ v → ↓ (Gt F') (r v)) d →
                      geqFMu F' r F'' U d y → 
                      ↓ (Gt F') y
               khe {F'' = Param} d ip (inj₁ eq) = ↓GtEq ip eq
               khe{n''}{F''} d ip (inj₂ dec) = kh{n''}{F''} d ip dec
-}           k{n'}{F'} ip rch (gtCong {R} r d {R'} r' d' U revealU ic rch') = ?
             j{n'}{F'} (interpPredCong{n'}{F'} (λ u d → ↓> d (revealU u)) ic ip) (reachesParamCong{n'}{F'} d d' ic rch')
-}