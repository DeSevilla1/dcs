open import lib

open import FunctorCodes

module Sized where

{-
 data EqF : ∀{n : ℕ}{p : Pol}(F : Fnc n p) → 

 module Eq(n : ℕ)(F : Fnc n pos)
  where

  data _≻_ : D → D → Set where
    Cong : ∀{fd fd'} → flift (_≻_) fd fd' → (inn fd) ≻ (inn fd')

{-
  {-# NO_POSITIVITY_CHECK #-}
  data _≈_ : D → D → Set where
    Cong : ∀{fd fd'} → flift (_≈_) fd fd' → (inn fd) ≈ (inn fd')

  {-# NO_POSITIVITY_CHECK #-}
  data _≻_ : D → D → Set where
    Cong : ∀{fd fd'} → flift (_≻_) fd fd' → (inn fd) ≻ (inn fd')


  -}-}