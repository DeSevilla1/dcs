open import lib 

module FunctorCodes where

{- Fnc n pos is type for codes for functors with n occurrences of Param -}
data Fnc : ℕ → Set₁ where
  Sum : ∀{n n' : ℕ} → (F₁ : Fnc n) → (F₂ : Fnc n') → Fnc (n + n')
  Prod : ∀{n n' : ℕ} → (F₁ : Fnc n) → (F₂ : Fnc n') → Fnc (n + n')
  Arrow : ∀{n : ℕ} → (X : Set) → (F : Fnc n) → Fnc n
  Param : Fnc 1
  Other : (X : Set) → Fnc 0

NatF : Fnc 1
NatF = Sum (Other ⊤) Param
              
ListF : Set → Fnc 1
ListF A = Sum (Other ⊤)
              (Prod (Other A)
                    Param)

TreeF : Set → Fnc 2
TreeF A = Sum (Other ⊤)
              (Prod (Other A)
              (Prod Param Param))

{- the standard interpretation of a Fnc, as a Set to Set function,
   where all params are interpreted as the sole input Set. -}
interp : ∀{n : ℕ} → Fnc n → Set → Set
interp (Sum f₁ f₂) v = interp f₁ v ⊎ interp f₂ v
interp (Prod f₁ f₂) v = interp f₁ v × interp f₂ v
interp (Arrow X f) v = X → interp f v
interp Param v = v
interp (Other X) v = X


{- the "multi-functor" interpretation, where each occurrence of Param is interpreted as
   a different type in an input vector. -}
interpMulti : ∀{n : ℕ} → Fnc n → 𝕍 Set n → Set
interpMulti (Sum{n₁}{n₂} f₁ f₂) v with splitAt𝕍 n₁ n₂ v
interpMulti (Sum{n₁}{n₂} f₁ f₂) v | (v₁ , v₂) = interpMulti{n₁} f₁ v₁ ⊎ interpMulti{n₂} f₂ v₂
interpMulti (Prod{n₁}{n₂} f₁ f₂) v with splitAt𝕍 n₁ n₂ v
interpMulti (Prod{n₁}{n₂} f₁ f₂) v | (v₁ , v₂) = interpMulti{n₁} f₁ v₁ × interpMulti{n₂} f₂ v₂
interpMulti (Arrow{n} X f) v = X → interpMulti{n} f v
interpMulti Param v = nth𝕍 0 refl v
interpMulti (Other X) v = X


{- interpret a Fnc as aredicate transformer, mapping

       base : V → V' → Set

   to a predicate of kind

       interp F V → interp F V' → Set

   stating that the F-structure of the two inputs is the same, and
   then the values of type V are related by base.
   
-}
interpCong : ∀{n : ℕ}(F : Fnc n){V V' : Set} →
            (V → V' → Set) →
            (x : interp F V)(x' : interp F V') →
            Set
interpCong (Sum F₁ F₂) base (inj₁ x₁) (inj₁ x₂) = interpCong F₁ base x₁ x₂
interpCong (Sum F₁ F₂) base (inj₁ x) (inj₂ y) = ⊥
interpCong (Sum F₁ F₂) base (inj₂ y) (inj₁ x) = ⊥
interpCong (Sum F₁ F₂) base (inj₂ y₁) (inj₂ y₂) = interpCong F₂ base y₁ y₂ 
interpCong (Prod F₁ F₂) base (a₁ , b₁) (a₂ , b₂) = interpCong F₁ base a₁ a₂ ∧ interpCong F₂ base b₁ b₂ 
interpCong (Arrow X F) base f₁ f₂ = ∀(x : X) → interpCong F base (f₁ x) (f₂ x) 
interpCong Param base x x' = base x x'
interpCong (Other x₁) base x x' = ⊤

interpPred : ∀{n : ℕ}(F : Fnc n){V : Set} →
            (V → Set) →
            (x : interp F V) →
            Set
interpPred (Sum F1 F2) p (inj₁ d) = interpPred F1 p d
interpPred (Sum F1 F2) p (inj₂ d) = interpPred F2 p d
interpPred (Prod F1 F2) p (d1 , d2) = interpPred F1 p d1 ∧ interpPred F2 p d2
interpPred (Arrow X F) p d = ∀{x : X} → interpPred F p (d x)
interpPred Param p d = p d
interpPred (Other X) p d = ⊤

reachesParam : ∀{n : ℕ}(F : Fnc n){V : Set}(x : interp F V) → Set
reachesParam (Sum F₁ F₂) (inj₁ x) = reachesParam F₁ x
reachesParam (Sum F₁ F₂) (inj₂ y) = reachesParam F₂ y
reachesParam (Prod F₁ F₂) (x₁ , x₂) = reachesParam F₁ x₁ ∨ reachesParam F₂ x₂ 
reachesParam (Arrow X F) f = Σ X λ x → reachesParam F (f x)
reachesParam Param x = ⊤ 
reachesParam (Other X) x = ⊥

reachesParamCong : ∀{n' : ℕ}{F' : Fnc n'} 
                    {R1 : Set}(d1 : interp F' R1)
                    {R2 : Set}(d2 : interp F' R2)
                    {U : R1 → R2 → Set} → 
                    interpCong F' U d1 d2 →
                    reachesParam F' d1 →                  
                    reachesParam F' d2
reachesParamCong {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) ic rch = reachesParamCong{F' = F'} d1 d2 ic rch
reachesParamCong {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) ic rch = reachesParamCong{F' = F''} d1 d2 ic rch
reachesParamCong {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₁ rch) =
      inj₁ (reachesParamCong{F' = F'} d1 d2 ic rch)
reachesParamCong {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₂ rch) =
       inj₂ (reachesParamCong{F' = F''} d1' d2' ic' rch)
reachesParamCong {F' = Arrow X F'} d1 d2 ic (x , rch) =
      (x , reachesParamCong{F' = F'} (d1 x) (d2 x) (ic x) rch)
reachesParamCong {F' = Param} d1 d2 ic rch = triv

reachesParamCong' : ∀{n' : ℕ}{F' : Fnc n'} 
                    {R1 : Set}(d1 : interp F' R1)
                    {R2 : Set}(d2 : interp F' R2)
                    {U : R1 → R2 → Set} → 
                    interpCong F' U d1 d2 →
                    reachesParam F' d2 →                  
                    reachesParam F' d1
reachesParamCong' {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) ic rch = reachesParamCong'{F' = F'} d1 d2 ic rch
reachesParamCong' {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) ic rch = reachesParamCong'{F' = F''} d1 d2 ic rch
reachesParamCong' {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₁ rch) =
      inj₁ (reachesParamCong'{F' = F'} d1 d2 ic rch)
reachesParamCong' {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₂ rch) =
       inj₂ (reachesParamCong'{F' = F''} d1' d2' ic' rch)
reachesParamCong' {F' = Arrow X F'} d1 d2 ic (x , rch) =
      (x , reachesParamCong'{F' = F'} (d1 x) (d2 x) (ic x) rch)
reachesParamCong' {F' = Param} d1 d2 ic rch = triv

interpPredCong : ∀{n : ℕ}{F : Fnc n}{V V' : Set}
                  {U : V → V' → Set}
                  {d : interp F V}{d' : interp F V'}
                  {P : V → Set}{P' : V' → Set} →
                  (∀{v : V}{v' : V'} →
                    U v v' → P v → P' v') →
                  interpCong F U d d' →
                  interpPred F P d →
                  interpPred F P' d'
interpPredCong {F = Sum F F'} {d = inj₁ d} {d' = inj₁ d'} g ic ip = interpPredCong{F = F} g ic ip
interpPredCong {F = Sum F F'} {d = inj₂ d} {d' = inj₂ d'} g ic ip = interpPredCong{F = F'} g ic ip
interpPredCong {F = Prod F F'} {d = (d1 , d1')} {d' = (d2 , d2')} g (ic , ic') (ip , ip') = 
  (interpPredCong{F = F} g ic ip , interpPredCong{F = F'} g ic' ip')
interpPredCong {F = Arrow X F} g ic ip {x} = interpPredCong{F = F} g (ic x) (ip{x}) 
interpPredCong {F = Param} g ic ip = g ic ip
interpPredCong {F = Other X} g ic ip = triv