open import lib

module SemTree where

data IndexTree : Set where
  Leaf : IndexTree
  Node : IndexTree → IndexTree → IndexTree

data TreeF(A X Y : Set) : Set where
  Leaf : TreeF A X Y
  Node : A → X → Y → TreeF A X Y

size : IndexTree → ℕ
size Leaf = 1
size (Node l r) = 1 + size l + size r

_≺_ : IndexTree → IndexTree → 𝔹
l ≺ r = size l < size r

ITreeF : Set → IndexTree → Set
ITreeF A Leaf = ⊥
ITreeF A (Node l r) = TreeF A (ITreeF A l) (ITreeF A r)

-- bounded
--ITreeB : Set → IndexTree → Set


removeLeft : ∀{i : IndexTree}{A : Set} → ITreeF A i → maybe (A × Σ IndexTree (λ i' → (i' ≺ i ≡ tt) × (ITreeF A i')))
removeLeft {Node l r} Leaf = nothing
removeLeft {Node (Node l1 l2) r} (Node x t1 t2) with removeLeft t1

removeLeft {Node (Node l1 l2) r} (Node x t1 t2) | Node y t1a t1b with 

removeLeft {Node (Node l1 l2) r} (Node x t1 t2) | Node y t1a t1b | (r , i' , p , s) = ?


removeLeft {Node (Node l1 l2) r} (Node x t1 t2) | Leaf = just (x , r , {!!} , t2)

Tree : Set → Set
Tree A = Σ IndexTree (ITreeF A)

