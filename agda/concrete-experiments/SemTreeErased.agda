open import lib

module SemTreeErased where

-- index is for nesting depth of functor applications
data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

data Tree(A : Set) : Set where
  Leaf : Tree A 
  Node : A → Tree A → Tree A → Tree A

data Etree : Set where
  Leaf : Etree A 
  Node : Etree A → Etree A → Etree A

erase : ∀{A : Set} → Tree A → Etree
erase Leaf = Leaf
erase (Node _ l r) = Node (erase l) (erase r)

-- 'loose'
Treel : ∀(A : Set) → Etree → Set
Treel A e = Σ (Tree A) (λ t' → )

Treet : ∀(A : Set) → (Tree A) → Set
Treet A t = Σ (Tree A) (λ t' → t >T t')

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 R2 L1 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg A R1 L1)
     (sfo2 : FoldT Alg A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

{-
{-
foldh : (n : ℕ) → ↓𝔹 _>_ n → {A : Set} → FoldT (SAlg A) A (Treel A (suc n)) (Treel A n)
foldh zero _ {A} func (mkSAlg alg) t =
  alg (SAlg A) (Treel A zero) ⊥ ⊥ ⊥ ⊥
    (λ t → Tzero t) (λ t → Tzero t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) (Tone t)
foldh (suc n) _ {A} func (mkSAlg alg) (m , p , Leaf) =
  alg (SAlg A) (Treel A (suc n)) ⊥ ⊥ ⊥ ⊥
    (λ _ _ → (1 , ≤0 n , Leaf)) (λ _ _ → (1 , ≤0 n , Leaf))
    (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) Leaf
foldh (suc n) (pf↓ w) {A} func (mkSAlg alg) (m , p , Node{n1}{n2} a l r) =
  let n1≤ = (≤-trans{n1}{n1 + n2}{n} (≤+1 n1 n2) p) in
  let n2≤ = (≤-trans{n2}{n1 + n2}{n} (≤+2 n1 n2) p) in
    alg (SAlg A) (Treel A (suc n)) (Treel A (suc n1)) (Treel A (suc n2)) (Treel A n1) (Treel A n2)
      (λ _ → TabstIn1{A}{n1}{n2}{n} p) (λ _ → TabstIn2{A}{n1}{n2}{n} p) Tsuc Tsuc (T≤ n1≤) (T≤ n2≤)
      (foldh n1 (w (≤<-trans{n1}{n}{suc n} n1≤ (<-suc n))))
      (foldh n2 (w (≤<-trans{n2}{n}{suc n} n2≤ (<-suc n))))
      id (mkSAlg alg) (Node a (suc n1 , ≤-refl (suc n1) , l) (suc n2 , ≤-refl (suc n2) , r))
-}-}