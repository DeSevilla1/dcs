open import lib

module SemTreeWf where

-- index is for nesting depth of functor applications
data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

data Tree(A : Set) : Set where
  Leaf : Tree A 
  Node : A → Tree A → Tree A → Tree A

mutual 

 data _>T_ {A : Set} : Tree A → Tree A → Set where
   >Nodel : ∀{a : A}{l l' r : Tree A} → l ≥T l' → Node a l r >T l'
   >Noder : ∀{a : A}{l r r' : Tree A} → r ≥T r' → Node a l r >T r'
   >Nodel' : ∀{a a' : A}{l l' r r' : Tree A} → l >T l' → r ≥T r' → Node a l r >T Node a' l' r'
   >Noder' : ∀{a a' : A}{l l' r r' : Tree A} → l ≥T l' → r >T r' → Node a l r >T Node a' l' r'


 _≥T_ : ∀{A : Set} → Tree A → Tree A → Set
 t ≥T t' = t >T t' ∨ t ≡ t'

↓->T : ∀{A : Set}(t : Tree A) → ↓ _>T_ t
↓->T{A} t = pf↓ (h t)
  where j : ∀ {a : A}{l r : Tree A} → ↓ _>T_ l → ↓ _>T_ r → ↓ _>T_ (Node a l r)
        j{a}{l}{r} u1 u2 = pf↓ (k u1 u2)
          where k : ∀ {a : A}{l r : Tree A} → ↓ _>T_ l → ↓ _>T_ r → ∀ {y} → (Node a l r) >T y → ↓ _>T_ y
                k u1 u2 (>Nodel (inj₁ u)) = ↓> u1 u
                k u1 u2 (>Nodel (inj₂ u)) rewrite u = u1
                k u1 u2 (>Noder (inj₁ u)) = ↓> u2 u
                k u1 u2 (>Noder (inj₂ u)) rewrite u = u2
                k (pf↓ f1) (pf↓ f2) (>Nodel' u (inj₁ u')) = j (f1 u) (f2 u')
                k (pf↓ f1) (pf↓ f2) (>Nodel' u (inj₂ p)) = j (f1 u) {!!}
                k (pf↓ f1) (pf↓ f2) (>Noder' (inj₁ u') u) = j (f1 u') (f2 u)
                k (pf↓ f1) (pf↓ f2) (>Noder' (inj₂ p) u) = j {!!} (f2 u)

        h : ∀ x → ∀ {y} → x >T y → ↓ _>T_ y
        h = {!!}
{-        h .(Node _ l _) (>Nodel{a}{l}{l'}{r} u) = ↓> (↓->T l) u
        h .(Node _ _ r) (>Noder{a}{l}{r}{r'} u) = ↓> (↓->T r) u
        h .(Node _ l _) (>Nodela{a}{l}{r}) = ↓->T l
        h .(Node _ _ r) (>Nodera{a}{l}{r}) = ↓->T r
        h .(Node _ _ _) (>Node{a}{a'}{l}{l'}{r}{r'} u1 u2) = j (h l u1) (h r u2) 
        h .(Node _ _ _) (>Nodelb{a}{a'}{l}{l'}{r} u) = j (h l u) (↓->T r)
        h .(Node _ _ _) (>Noderb{a}{a'}{l}{r}{r'} u) = j (↓->T l) (h r u) 
        -}

-- 'loose'
Treel : ∀(A : Set) → (Tree A) → Set
Treel A t = Σ (Tree A) (λ t' → t ≥T t')

Treet : ∀(A : Set) → (Tree A) → Set
Treet A t = Σ (Tree A) (λ t' → t >T t')

TLeaf : ∀{A : Set} → Treet A Leaf → ∀{X : Set} → X
TLeaf (t , ())

{-
>Leaf : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
        Node x t1 t2 >T Leaf
>Leaf {t1 = Leaf} = >Nodel
>Leaf {t1 = Node x t1 t2} = >Nodel (>Leaf{t1 = t1}) 

>trans : ∀{A : Set}{t1 t2 t3 : Tree A} → t1 >T t2 → t2 >T t3 → t1 >T t3
>trans p q = {!!}

TLeafl : ∀{A B : Set} → TreeF A B B
TLeafl = Leaf

TLeafin : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
          TreeF A ⊥ ⊥ → Treet A (Node x t1 t2)
TLeafin Leaf = (Leaf , >Leaf)

TreetRelax : ∀{A : Set}{t t' : Tree A} → Treet A t' → t ≥T t' → Treet A t
TreetRelax (u , p) (inj₁ q) = {!!}
TreetRelax{A}{t}{t'} (u , p) (inj₂ q) rewrite q = (u , p)

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 R2 L1 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg A R1 L1)
     (sfo2 : FoldT Alg A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X


foldh : ∀{A : Set} → (t : Tree A) → FoldT (SAlg A) A (Treel A t) (Treet A t)
foldh {A} Leaf func (mkSAlg alg) w = 
  alg (SAlg A) (Treet A Leaf) ⊥ ⊥ ⊥ ⊥
    (λ t → TLeaf t) (λ t → TLeaf t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) TLeafl
foldh {A} (Node x t1 t2) func (mkSAlg alg) (Leaf , inj₁ x₁) =
  alg (SAlg A) (Treet A (Node x t1 t2)) ⊥ ⊥ ⊥ ⊥
    (λ _ → TLeafin) (λ _ → TLeafin) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) TLeafl
foldh {A} (Node x t1 t2) func (mkSAlg alg) (Node x' t1' t2' , p) = {!!}

{-
{-
foldh : (n : ℕ) → ↓𝔹 _>_ n → {A : Set} → FoldT (SAlg A) A (Treel A (suc n)) (Treel A n)
foldh zero _ {A} func (mkSAlg alg) t =
  alg (SAlg A) (Treel A zero) ⊥ ⊥ ⊥ ⊥
    (λ t → Tzero t) (λ t → Tzero t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) (Tone t)
foldh (suc n) _ {A} func (mkSAlg alg) (m , p , Leaf) =
  alg (SAlg A) (Treel A (suc n)) ⊥ ⊥ ⊥ ⊥
    (λ _ _ → (1 , ≤0 n , Leaf)) (λ _ _ → (1 , ≤0 n , Leaf))
    (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) Leaf
foldh (suc n) (pf↓ w) {A} func (mkSAlg alg) (m , p , Node{n1}{n2} a l r) =
  let n1≤ = (≤-trans{n1}{n1 + n2}{n} (≤+1 n1 n2) p) in
  let n2≤ = (≤-trans{n2}{n1 + n2}{n} (≤+2 n1 n2) p) in
    alg (SAlg A) (Treel A (suc n)) (Treel A (suc n1)) (Treel A (suc n2)) (Treel A n1) (Treel A n2)
      (λ _ → TabstIn1{A}{n1}{n2}{n} p) (λ _ → TabstIn2{A}{n1}{n2}{n} p) Tsuc Tsuc (T≤ n1≤) (T≤ n2≤)
      (foldh n1 (w (≤<-trans{n1}{n}{suc n} n1≤ (<-suc n))))
      (foldh n2 (w (≤<-trans{n2}{n}{suc n} n2≤ (<-suc n))))
      id (mkSAlg alg) (Node a (suc n1 , ≤-refl (suc n1) , l) (suc n2 , ≤-refl (suc n2) , r))
-}-}-}