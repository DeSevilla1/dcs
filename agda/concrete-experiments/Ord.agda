open import lib

module Ord where

data Ord : Set where
  zero : Ord
  succ : Ord → Ord
  lim : (ℕ → Ord) → Ord

postulate
  _≥O_ : Ord → Ord → Set

data _>O_ : Ord → Ord → Set where
  >lim : ∀{f f' : ℕ → Ord}(m : ℕ) → f m >O f' m → (∀(m : ℕ) → f m ≥O f' m) → lim f >O lim f'