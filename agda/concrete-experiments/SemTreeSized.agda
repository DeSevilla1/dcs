open import lib

module SemTreeSized where

data Tree(A : Set) : ℕ → Set where
  Leaf : ∀{n : ℕ} → Tree A n
  Node : ∀{n : ℕ} → A → Tree A n → Tree A n → Tree A (suc n)

-- 'loose'
Treel : Set → ℕ → Set
Treel A n = Σ ℕ (λ m → m ≤ n ≡ tt ∧ Tree A m)

leafl : ∀{A} → Treel A 0
leafl = (0 , refl , Leaf)

id : ∀{X : Set} → X → X
id x = x

data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R L : Set)
     (abstIn1 : R → TreeF A L R → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R L → R) -- similarly
     (up : L → R)
     (sfo1 : FoldT Alg A R L)
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R R) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

foldh : (n : ℕ){A : Set} → FoldT (SAlg A) A (Treel A n) (Treel A (pred n))
foldh (suc (suc n)){A} func (mkSAlg alg) t =
 alg (SAlg A) (Treel A (suc n)) (Treel A n)
    {!!} {!!} {!!} (foldh (suc n)) id (mkSAlg alg) {!!}
foldh (suc zero){A} func (mkSAlg alg) t = {!!}
foldh zero{A} func (mkSAlg alg) t = {!!}

{-
foldh 0{A} func (mkSAlg alg) t =
  alg (SAlg A) (Treel A 0) ⊤ ⊥ ⊤ ⊥ (λ x _ → x) (λ x _ → x) (λ _ → leafl) (λ _ → leafl) (λ()) (λ()) (λ _ _ x → {!!})  {!!} id (mkSAlg alg) {!!} 
foldh (suc n){A} func (mkSAlg alg) t =
  alg (SAlg A) {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} 
-}
{-
foldh : (n : ℕ)(e : TreeE) → size e ≡ (suc n) → {A : Set} → FoldT (SAlg A) A (TreeT A e) 
foldh 0 (Node l r) p {A} func (mkSAlg alg) x = alg (SAlg A) {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} ? ? ? ? ? ?
foldh (suc n) e p {A} func (mkSAlg alg) x = {!!} 
-}

{-
data Tree(A : Set) : ℕ → Set where
  Leaf : ∀{n : ℕ} → Tree A (suc n)
  Node : ∀{n1 n2 : ℕ} → A → Tree A n1 → Tree A n2 → Tree A (suc (n1 + n2))

removeLeft : ∀{n : ℕ}{A : Set} → Tree A (suc n) → maybe (A × Tree A n)
removeLeft Leaf = nothing
removeLeft (Node x l r) with l
removeLeft (Node x l r) | Node x1 l1 r1 with removeLeft l
removeLeft (Node x l r) | _ | just (y , l') = just (y , Node x l' r)
removeLeft (Node x l r) | _ | nothing = just (x , {!!}) 
removeLeft (Node x l r) | Leaf = just (x , {!!})
-}