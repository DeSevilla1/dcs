open import lib
open import Iter

module SemNat where

data NatF(X : Set) : Set where
  Zero : NatF X
  Suc  : X → NatF X

INatF : ℕ → Set
INatF = IterT NatF ⊥

Nat : Set
Nat = Σ ℕ INatF

In : NatF Nat → Nat
In Zero = (1 , Zero)
In (Suc (n , v)) = (suc n , Suc v)

Out : Nat → NatF Nat
Out (zero , v) = Zero
Out (suc n , Zero) = Zero
Out (suc n , Suc v) = Suc (n , v)

Carrier : Set₁
Carrier = Set → Set

Functor : Carrier → Set₁
Functor C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set₁
FoldT Alg R = ∀{X : Carrier} → Functor X → Alg X → R → Σ Set (λ L → (NatF L → R) × X L)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           (∀ (A : SAlgK)(R : Set)
              (sfo : FoldT A R) →
              (embedAlg : ∀{Y : Carrier} → SAlg Y → A Y) → 
              (d : NatF R) → 
              X R) → 
           SAlg X

id : ∀{X : Set} → X → X
id x = x

foldh : (n : ℕ) → FoldT SAlg (INatF n)
foldh (suc n) _ (mkSAlg salg) v =
  (INatF n , id , salg SAlg (INatF n) (foldh n) (λ a → a) v )

fold : FoldT SAlg Nat 
fold fmap a (suc n , v) with foldh (suc n) fmap a v
fold fmap a (suc n , v) | (L , e , r) =
  (L , (λ u → (suc n , e u)) , r)