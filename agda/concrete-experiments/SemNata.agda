open import lib
open import Iter

module SemNata where

data NatF(X : Set) : Set where
  Zero : NatF X
  Suc  : X → NatF X

INatF : ℕ → Set
INatF = IterT NatF ⊥

Nat : Set
Nat = Σ ℕ INatF

In : NatF Nat → Nat
In Zero = (1 , Zero)
In (Suc (n , v)) = (suc n , Suc v)

Out : Nat → NatF Nat
Out (zero , v) = Zero
Out (suc n , Zero) = Zero
Out (suc n , Suc v) = Suc (n , v)

Carrier : Set₁
Carrier = Set → Set → Set

Bifunctor : Carrier → Set₁
Bifunctor C = ∀{A A' B B' : Set} → (A → A') → (B → B') → C A B → C A' B'

SAlgK : Set₁
SAlgK = Carrier → Set

{-
FoldT : SAlgK → Set → Set₁
FoldT Alg C = ∀{X : Carrier} → Bifunctor X → Alg X → C → X C C
-}

FoldT : SAlgK → Set → Set → Set₁
FoldT Alg L R = ∀{X : Carrier} → Bifunctor X → Alg X → R → X L R 

{-
{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           (∀ (R P : Set) (up : R → P)
              (sfo : FoldT SAlg R) →
              (abstIn : NatF R → P)
              (rec : R → X R R)
              (d : NatF R) → 
              X R P) → 
           SAlg X
-}

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           (∀ (L R P : Set)
              (up1 : L → R)
              (up2 : R → P)
              (sfo : FoldT SAlg L R) →
              (abstIn2 : NatF R → P)
              (rec : R → X L R)
              (d : NatF R) → 
              X R P) → 
           SAlg X


id : ∀{X : Set} → X → X
id x = x

foldh : (n : ℕ) → ∀ {X : Carrier} → Bifunctor X → SAlg X → INatF (suc n) → X (INatF n) (INatF (suc n))
foldh zero bi (mkSAlg salg) v =
 salg ⊥ ⊥ (NatF ⊥) id (λ()) (λ _ _ ()) id (λ()) v
foldh (suc n) bi (mkSAlg salg) v = 
 salg (INatF n) (INatF (suc n)) (INatF (suc (suc n))) {!!} {!!} (foldh n) id (foldh n bi (mkSAlg salg)) v

fold : FoldT SAlg Nat Nat
fold bi a (suc n , v) = bi (λ v → (n , v)) (λ v → (suc n , v)) (foldh n bi a v)

{-
looks good, but does not decrease R:

foldh : (n : ℕ) → ∀ {X : Carrier} → Bifunctor X → SAlg X → INatF n → X (INatF n) (INatF n)
foldh (suc n) bi (mkSAlg salg) v =
  bi {!!} {!!} (salg (INatF n) (INatF (suc n)) {!!} (foldh n) (λ x → x) (foldh n bi (mkSAlg salg)) v)


fold : FoldT SAlg Nat
fold bi a (n , v) = bi {!!} {!!} (foldh n bi a v)
-}

