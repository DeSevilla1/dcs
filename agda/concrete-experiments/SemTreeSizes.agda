-- index the Tree type by size

open import lib

module SemTreeSizes where

data TreeF(A : Set)(R1 R2 : ℕ → Set) : ℕ → Set where
  Leaf : ∀{n : ℕ} → TreeF A R1 R2 n
  Node : ∀{n1 n2 : ℕ} → A → R1 n1 → R2 n2 → TreeF A R1 R2 (suc (n1 + n2))

Tree : Set → ℕ → Set
Tree A zero = ⊥
Tree A (suc n) = TreeF A (Tree A) (Tree A) n

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set₁
FoldT Alg A R = ∀{X : Carrier} → Func X → Alg X → R → X R

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 R2 : Set)
     (abstIn : R → TreeF A R1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R 
     (up1 : R1 → R)
     (up2 : R2 → R)     
     (sfo1 : FoldT Alg A R1)
     (sfo2 : FoldT Alg A R2)      
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

{-
foldh : (n : ℕ){A : Set} → FoldT (SAlg A) A (TreeF A (Node l r))
foldh Leaf r {A} func (mkSAlg alg) x = salg (SAlg A) (TreeT A (Node l r))
foldh l Leaf {A} func (mkSAlg alg) x = {!!} 
foldh (Node l1 l2) (Node r1 r2) {A} func (mkSAlg alg) x = {!!} 


{-
data Tree(A : Set) : ℕ → Set where
  Leaf : ∀{n : ℕ} → Tree A (suc n)
  Node : ∀{n1 n2 : ℕ} → A → Tree A n1 → Tree A n2 → Tree A (suc (n1 + n2))

removeLeft : ∀{n : ℕ}{A : Set} → Tree A (suc n) → maybe (A × Tree A n)
removeLeft Leaf = nothing
removeLeft (Node x l r) with l
removeLeft (Node x l r) | Node x1 l1 r1 with removeLeft l
removeLeft (Node x l r) | _ | just (y , l') = just (y , Node x l' r)
removeLeft (Node x l r) | _ | nothing = just (x , {!!}) 
removeLeft (Node x l r) | Leaf = just (x , {!!})
-}-}