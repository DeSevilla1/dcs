open import lib

module SemTreeWf2Levels where

-- index is for nesting depth of functor applications
data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

data Tree(A : Set) : Set where
  Leaf : Tree A 
  Node : A → Tree A → Tree A → Tree A

mutual 

 data _≥T_ {A : Set} : Tree A → Tree A → Set where
  ≥refl : ∀{t t' : Tree A} → t ≡ t' → t ≥T t'
  ≥Node : ∀{a a' : A}{l l' r r' : Tree A} → l ≥T l' → r ≥T r' → Node a l r ≥T Node a' l' r'
  ≥Nodel : ∀{a : A}{l l' r : Tree A} → l ≥T l' → Node a l r ≥T l'
  ≥Noder : ∀{a : A}{l r r' : Tree A} → r ≥T r' → Node a l r ≥T r'

 data _>T_ {A : Set} : Tree A → Tree A → Set where
  >Nodel : ∀{a : A}{l l' r : Tree A} → l ≥T l' → Node a l r >T l'
  >Noder : ∀{a : A}{l r r' : Tree A} → r ≥T r' → Node a l r >T r'
  >Node : ∀{a a' : A}{l l' r r' : Tree A} → l >T l' → r ≥T r' → Node a l r >T Node a' l' r'
  >Noderb : ∀{a a' : A}{l r r' : Tree A} → r >T r' → Node a l r >T Node a' l r'

↓->T : ∀{A : Set}(t : Tree A) → ↓ _>T_ t
↓->T{A} t = pf↓ (h t)
  where j : ∀ {a : A}{l r : Tree A} → ↓ _>T_ l → ↓ _>T_ r → ↓ _>T_ (Node a l r)
        j{a}{l}{r} u1 u2 = pf↓ (k u1 u2)
          where k : ∀ {a : A}{l r : Tree A} → ↓ _>T_ l → ↓ _>T_ r → ∀ {y} → (Node a l r) >T y → ↓ _>T_ y
                k u1 u2 (>Nodel{l = la} u) = {!!} --↓> u1 u
                k u1 u2 (>Noder{r = ra} u) = {!!} -- ↓> u2 u
                k (pf↓ f1) (pf↓ f2) (>Node{a}{a'} u u') = {!!} --j (f1 u) (f2 u') 
                k u1 (pf↓ f2) (>Noderb u) = j u1 (f2 u)
        h : ∀ x → ∀ {y} → x >T y → ↓ _>T_ y
        h .(Node _ l _) (>Nodel{a}{l}{l'}{r} u) = {!!} --↓> (↓->T l) u
        h .(Node _ _ r) (>Noder{a}{l}{r}{r'} u) = {!!} -- ↓> (↓->T r) u
        h .(Node _ _ _) (>Node{a}{a'}{l}{l'}{r}{r'} u1 u2) = {!!} --j (h l u1) (h r u2) 
        h .(Node _ _ _) (>Noderb{a}{a'}{l}{r}{r'} u) = j (↓->T l) (h r u) 
        
{-

-- 'loose'
Treel : ∀(A : Set) → (Tree A) → Set
Treel A t = Σ (Tree A) (λ t' → t ≥T t')

Treet : ∀(A : Set) → (Tree A) → Set
Treet A t = Σ (Tree A) (λ t' → t >T t')

TLeaf : ∀{A : Set} → Treet A Leaf → ∀{X : Set} → X
TLeaf (t , ())

>Leaf : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
        Node x t1 t2 >T Leaf
>Leaf {t1 = Leaf} = >Nodela
>Leaf {t1 = Node x t1 t2} = >Nodel (>Leaf{t1 = t1}) 

>trans : ∀{A : Set}{t1 t2 t3 : Tree A} → t1 >T t2 → t2 >T t3 → t1 >T t3
>trans (>Nodel p) q = >Nodel (>trans p q)
>trans (>Noder p) q = >Noder (>trans p q)
>trans >Nodela q = >Nodel q
>trans >Nodera q = >Noder q
>trans (>Node p p₁) (>Nodel q) = >Nodel (>trans p q)
>trans (>Node p p') (>Noder q) = >Noder (>trans p' q)
>trans (>Node p p₁) >Nodela = >Nodel p
>trans (>Node p p') >Nodera = >Noder p'
>trans (>Node p p') (>Node q q') = >Node (>trans p q) (>trans p' q')
>trans (>Node p p') (>Nodelb q) = >Node (>trans p q) p'
>trans (>Node p p') (>Noderb q) = >Node p (>trans p' q)
>trans (>Nodelb p) (>Nodel q) = >Nodel (>trans p q)
>trans (>Nodelb p) (>Noder q) = >Noder q
>trans (>Nodelb p) >Nodela = >Nodel p
>trans (>Nodelb p) >Nodera = >Nodera
>trans (>Nodelb p) (>Node q q') = >Node (>trans p q) q'
>trans (>Nodelb p) (>Nodelb q) = >Nodelb (>trans p q)
>trans (>Nodelb p) (>Noderb q) = >Node p q
>trans (>Noderb p) (>Nodel q) = >Nodel q
>trans (>Noderb p) (>Noder q) = >Noder (>trans p q)
>trans (>Noderb p) >Nodela = >Nodela
>trans (>Noderb p) >Nodera = >Noder p
>trans (>Noderb p) (>Node q q') = >Node q (>trans p q')
>trans (>Noderb p) (>Nodelb q) = >Node q p
>trans (>Noderb p) (>Noderb q) = >Noderb (>trans p q)

≥>trans : ∀{A : Set}{t1 t2 t3 : Tree A} → t1 ≥T t2 → t2 >T t3 → t1 >T t3
≥>trans (inj₁ p) q = >trans p q
≥>trans (inj₂ p) q rewrite p = q

TLeafin : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
          TreeF A ⊥ ⊥ → Treet A (Node x t1 t2)
TLeafin Leaf = (Leaf , >Leaf)

TreetRelax : ∀{A : Set}{t t' : Tree A} → t ≥T t' → Treet A t' → Treet A t
TreetRelax (inj₁ q) (u , p) = (u , >trans q p)
TreetRelax{A}{t}{t'} (inj₂ q) (u , p) rewrite q = (u , p)

Treetl : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
         TreeF A (Treet A t1) (Treel A t2) → Treet A (Node x t1 t2)
Treetl Leaf = TLeafin Leaf
Treetl {A} {x'} (Node x (l , p) (r , inj₁ q)) = (Node x l r , >Node p q)
Treetl {A} {x'} (Node x (l , p) (r , inj₂ q)) rewrite q = (Node x l r , >Nodelb p)

Treetr : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
         TreeF A (Treel A t1) (Treet A t2) → Treet A (Node x t1 t2)
Treetr Leaf = TLeafin Leaf
Treetr {A} {x'} (Node x (l , inj₁ p) (r , q)) = (Node x l r , >Node p q)
Treetr {A} {x'} (Node x (l , inj₂ p) (r , q)) rewrite p = (Node x l r , >Noderb q)

Treell : ∀{A : Set}{x : A}{t1 t2 : Tree A} →
         TreeF A (Treel A t1) (Treel A t2) → Treel A (Node x t1 t2)
Treell Leaf = (Leaf , inj₁ >Leaf)
Treell {A} {x'} (Node x (l , inj₁ x₁) (r , inj₁ x₂)) = (Node x l r , inj₁ (>Node x₁ x₂))
Treell {A} {x'} (Node x (l , inj₁ x₁) (r , inj₂ y)) rewrite y = (Node x l r , inj₁ (>Nodelb x₁))
Treell {A} {x'} (Node x (l , inj₂ y) (r , inj₁ x₁)) rewrite y = (Node x l r , inj₁ (>Noderb x₁))
Treell {A} {x'} (Node x (l , inj₂ y) (r , inj₂ y₁)) rewrite y | y₁ = (Node {!!} l r , inj₂ refl)

Treet2l : ∀{A : Set}{t : Tree A} → Treet A t → Treel A t
Treet2l (t' , p) = (t' , inj₁ p)

TreelNodel : ∀{A : Set}{x : A}{t1 t2 : Tree A} → Treel A t1 → Treet A (Node x t1 t2)
TreelNodel (t1' , inj₁ p) = (t1' , >Nodel p)
TreelNodel (t1' , inj₂ p) rewrite p = (t1' , >Nodela)

TreelNoder : ∀{A : Set}{x : A}{t1 t2 : Tree A} → Treel A t2 → Treet A (Node x t1 t2)
TreelNoder (t2' , inj₁ p) = (t2' , >Noder p)
TreelNoder (t2' , inj₂ p) rewrite p = (t2' , >Nodera)

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

AlgK : Set₁
AlgK = Carrier → Set

FoldT : AlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

Alg1Body : AlgK → Carrier → Set → Set₁
Alg1Body Alg1' X A =
  (∀ (Alg1 : AlgK)(R R1 R2 L1 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg1 A R1 L1)
     (sfo2 : FoldT Alg1 A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg1' Y → Alg1 Y)
     (alg : Alg1 X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data Alg1(A : Set) : AlgK where
  mkAlg1 : ∀ {X : Carrier} →
           Alg1Body (Alg1 A) X A → 
           Alg1 A X

foldh : ∀{A : Set} → (t : Tree A) → ↓ _>T_ t → FoldT (Alg1 A) A (Treel A t) (Treet A t)
foldh {A} Leaf _ func (mkAlg1 alg) w = 
  alg (Alg1 A) (Treet A Leaf) ⊥ ⊥ ⊥ ⊥
    (λ t → TLeaf t) (λ t → TLeaf t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkAlg1 alg) Leaf
foldh {A} (Node x t1 t2) _ func (mkAlg1 alg) (Leaf , inj₁ x₁) =
  alg (Alg1 A) (Treet A (Node x t1 t2)) ⊥ ⊥ ⊥ ⊥
    (λ _ → TLeafin) (λ _ → TLeafin) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkAlg1 alg) Leaf
foldh {A} (Node x t1 t2) (pf↓ wf) func (mkAlg1 alg) (Node x' t1' t2' , p) =
  func (TreetRelax p)
   (alg (Alg1 A) (Treet A (Node x' t1' t2')) (Treel A t1') (Treel A t2') (Treet A t1') (Treet A t2')
     (λ _ t → Treetl t) (λ _ t → Treetr t) Treet2l Treet2l
     TreelNodel TreelNoder
     (foldh t1' (wf (≥>trans p >Nodela)))
     (foldh t2' (wf (≥>trans p >Nodera)))
     id (mkAlg1 alg) (Node x' (t1' , inj₂ refl) (t2' , inj₂ refl)))

----------------------------------------------------------------------
-- second level 

Alg2Body : AlgK → Carrier → Set → Set₁
Alg2Body Alg2' X A =
  (∀ (Alg2 : AlgK)(R R1 R2 L1 L2 : Set)
     (foldTop : FoldT (Alg1 A) A (TreeF A R1 R2) R)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg2 A R1 L1)
     (sfo2 : FoldT Alg2 A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg2' Y → Alg2 Y)
     (alg : Alg2 X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data Alg2(A : Set) : AlgK where
  mkAlg2 : ∀ {X : Carrier} →
           Alg2Body (Alg2 A) X A → 
           Alg2 A X

foldh2 : ∀{A : Set} → (t : Tree A) → ↓ _>T_ t → FoldT (Alg2 A) A (Treel A t) (Treet A t)
foldh2 {A} Leaf wf func (mkAlg2 alg) w = 
  alg (Alg2 A) (Treet A Leaf) ⊥ ⊥ ⊥ ⊥ (λ fnc al u → foldh Leaf wf fnc al (Leaf , inj₂ refl))
    (λ t → TLeaf t) (λ t → TLeaf t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkAlg2 alg) Leaf
foldh2 {A} (Node x t1 t2) wf func (mkAlg2 alg) (Leaf , inj₁ x₁) =
  alg (Alg2 A) (Treet A (Node x t1 t2)) ⊥ ⊥ ⊥ ⊥ (λ fnc al u → foldh{A} (Node x t1 t2) wf fnc al (Leaf , inj₁ >Leaf))
    (λ _ → TLeafin) (λ _ → TLeafin) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkAlg2 alg) Leaf
foldh2 {A} (Node x t1 t2) (pf↓ wf) func (mkAlg2 alg) (Node x' t1' t2' , p) =
  func (TreetRelax p)
   (alg (Alg2 A) (Treet A (Node x' t1' t2')) (Treel A t1') (Treel A t2') (Treet A t1') (Treet A t2')
     (λ fnc al u → foldh{A} (Node x' t1' t2') (↓≥ (pf↓ wf) p) fnc al (Treell u))
     (λ _ t → Treetl t) (λ _ t → Treetr t) Treet2l Treet2l
     TreelNodel TreelNoder
     (foldh2 t1' (wf (≥>trans p >Nodela)))
     (foldh2 t2' (wf (≥>trans p >Nodera)))
     id (mkAlg2 alg) (Node x' (t1' , inj₂ refl) (t2' , inj₂ refl)))
-}