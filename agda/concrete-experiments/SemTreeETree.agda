{- like SemTreeSized2, but indexed by ETrees, which are
   like Trees but with data erased (hence the "E") -}

open import lib

module SemTreeETree where

data ETree : Set where
  Leaf : ETree
  Node : ETree → ETree → ETree

size : ETree → ℕ
size Leaf = 1
size (Node n m) = suc (size n + size m)

-- index is for nesting depth of functor applications
data Tree(A : Set) : ETree → Set where
  Leaf : Tree A Leaf
  Node : ∀{n m : ETree} → A → Tree A n → Tree A m → Tree A (Node n m)

_≤E_ : ETree → ETree → Set
m ≤E n = size m ≤ size n ≡ tt 

_<E_ : ETree → ETree → Set
m <E n = size m < size n ≡ tt 

_>E_ : ETree → ETree → Set
m >E n = size m > size n ≡ tt 

¬Node≤Leaf : ∀{x y : ETree} → Node x y ≤E Leaf → ∀{X : Set} → X
¬Node≤Leaf {Leaf} {Leaf} () 
¬Node≤Leaf {Leaf} {Node y y₁} () 
¬Node≤Leaf {Node x x₁} {Leaf} () 
¬Node≤Leaf {Node x x₁} {Node y y₁} ()

¬Node<Leaf : ∀{x y : ETree} → Node x y <E Leaf → ∀{X : Set} → X
¬Node<Leaf {Leaf} {Leaf} () 
¬Node<Leaf {Leaf} {Node y y₁} () 
¬Node<Leaf {Node x x₁} {Leaf} () 
¬Node<Leaf {Node x x₁} {Node y y₁} ()

-- 'loose'
Treel : Set → ETree → Set
Treel A n = Σ ETree (λ m → m ≤E n ∧ Tree A m)

Treet : Set → ETree → Set
Treet A n = Σ ETree (λ m → m <E n ∧ Tree A m)

TreetLeaf : ∀ {A : Set} → Treet A Leaf → ∀{X : Set} → X
TreetLeaf (Node x y , p , t) = ¬Node<Leaf{x}{y} p

data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2


id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 R2 L1 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg A R1 L1)
     (sfo2 : FoldT Alg A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

foldh : (n : ETree) → ↓ _>E_ n → {A : Set} → FoldT (SAlg A) A (Treel A n) (Treet A n)
foldh Leaf wf {A} fnc (mkSAlg alg) (Leaf , p , t) =
  alg (SAlg A) (Treet A Leaf) ⊥ ⊥ ⊥ ⊥
    (λ u → TreetLeaf u) (λ u → TreetLeaf u) (λ())(λ())(λ())(λ()) (λ _ _ ())(λ _ _ ()) id (mkSAlg alg) Leaf
foldh Leaf wf {A} {X} fnc (mkSAlg alg) (Node x y , p , t) = ¬Node≤Leaf{x}{y} p 
foldh (Node n n') wf fnc (mkSAlg alg) (m , p , t) = {!!}