open import lib

module VecTree where

data VecTree(n : ℕ) : Set where
  Leaf : VecTree n
  Node : 𝕍 (VecTree n) n → VecTree n

