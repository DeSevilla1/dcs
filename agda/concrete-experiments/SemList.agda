open import lib
open import Iter

module SemList where

data ListF(A X : Set) : Set where
  Nil : ListF A X
  Cons  : A → X → ListF A X

IListF : Set → ℕ → Set
IListF A = IterT (ListF A) ⊥

List : Set → Set
List A = Σ ℕ (IListF A)

upIListF : ∀{n : ℕ}{A : Set} → IListF A n → IListF A (suc n)
upIListF {suc n} Nil = Nil
upIListF {suc n} (Cons x xs) = Cons x (upIListF{n} xs)

upIListF+ : ∀{n m : ℕ}{A : Set} → IListF A n → IListF A (n + m)
upIListF+ {n}{zero} xs rewrite +0 n = xs
upIListF+ {n}{suc m} xs rewrite +suc n m = upIListF{n + m} (upIListF+{n}{m} xs)

In : ∀{A : Set} → ListF A (List A) → List A
In Nil = (1 , Nil)
In (Cons a (n , v)) = (suc n , Cons a v)

Out : ∀{A : Set} → List A → ListF A (List A)
Out (zero , v) = Nil
Out (suc n , Nil) = Nil
Out (suc n , Cons a v) = Cons a (n , v)

nil : ∀{A : Set} → List A
nil = In Nil

cons : ∀{A : Set} → A → List A → List A
cons a xs = In (Cons a xs)

id : ∀{X : Set} → X → X
id x = x

Carrier : Set₁
Carrier = Set → Set

Functor : Carrier → Set₁
Functor C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A L R = ∀{X : Carrier} → Functor X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(L R : Set)
     (abstIn : R → ListF A L → R) -- the R input is needed to block nontermination in base case of foldh
     (up : R → ListF A R)
     (sfo : FoldT Alg A L R) 
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : ListF A R) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

foldh : (n : ℕ){A : Set} → FoldT (SAlg A) A (IListF A n) (IListF A (suc n))
foldh zero {A} _ (mkSAlg salg) l = salg (SAlg A) ⊥ ⊥ (λ()) (λ()) (λ _ _ ()) id (mkSAlg salg) l
foldh (suc n) {A} _ (mkSAlg salg) l =
  salg (SAlg A) (IListF A n) (IListF A (suc n)) (λ _ → id) (upIListF{suc n}) (foldh n) id (mkSAlg salg) l

fold : ∀{A : Set} → FoldT (SAlg A) A (List A) (List A)
fold fmap a (suc n , v) = fmap (λ v → (n , v)) (foldh n fmap a v)

----------------------------------------------------------------------
-- example: mergesort

OutC : Set → Carrier
OutC A R = ListF A R

OutCFun : ∀(A : Set) → Functor (OutC A)
OutCFun = {!!}

out : ∀{A : Set} → SAlg A (OutC A)
out = mkSAlg (λ Alg L R abstIn up sfo embedAlg alg d → d)

Split : Set → Carrier
Split A R = (ListF A R) × (ListF A R)

SplitFun : ∀{A : Set} → Functor (Split A)
SplitFun = {!!}

split : ∀{A : Set} → SAlgBody (SAlg A) (Split A) A
split Alg L R abstIn up sfo embedAlg self xs with xs
split Alg L R abstIn up sfo embedAlg self xs | Nil = (Nil , Nil)
split{A} Alg L R abstIn up sfo embedAlg self xs | Cons x t with sfo (OutCFun A) (embedAlg out) t
split{A} Alg L R abstIn up sfo embedAlg self xs | Cons x t | t' with t'
split{A} Alg L R abstIn up sfo embedAlg self xs | Cons x t | t' | Nil = (xs , up (abstIn t t'))
split{A} Alg L R abstIn up sfo embedAlg self xs | Cons x t | _ | Cons y t' with sfo SplitFun self t
split{A} Alg L R abstIn up sfo embedAlg self xs | Cons x t | _ | Cons y t' | (l , r) = (Cons x (abstIn t l) , Cons y (abstIn t r))

Msort : Set → Carrier
Msort A _ = A → List A

MsortFun : ∀(A : Set) → Functor (Msort A)
MsortFun = {!!}

msort : ∀{A : Set} → SAlgBody (SAlg A) (Msort A) A
msort Alg L R abstIn up sfo embedAlg self xs a with xs
msort Alg L R abstIn up sfo embedAlg self xs a | Nil = nil
msort{A} Alg L R abstIn up sfo embedAlg self xs a | Cons x t with sfo SplitFun (embedAlg (mkSAlg split)) t
msort{A} Alg L R abstIn up sfo embedAlg self xs a | Cons x t | l , r =
  sfo (MsortFun A) self (abstIn t l) a -- don't bother merging, as this is just a demo

----------------------------------------------------------------------
-- reverse with sizes



rev : ∀{n m : ℕ}{A : Set} → IListF A n → IListF A m → IListF A (n + m)
rev{suc n}{m} Nil ys rewrite +comm n m = upIListF{m + n} (upIListF+{m}{n} ys)
rev{suc n}{m}{A} (Cons x xs) ys with rev{n}{suc m} xs (Cons x ys) 
rev{suc n}{m}{A} (Cons x xs) ys | p rewrite +suc n m = p



{-

    -}