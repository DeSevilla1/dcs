open import lib

module Iter where

-- could make level-polymorphic
IterT : (Set → Set) → Set → ℕ → Set
IterT F X zero = X
IterT F X (suc n) = F (IterT F X n)