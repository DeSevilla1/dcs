open import lib
open import Iter

module SemNat where

data NatF(X : Set) : Set where
  Zero : NatF X
  Suc  : X → NatF X

INatF : ℕ → Set
INatF = IterT NatF ⊥

Nat : Set
Nat = Σ ℕ INatF

In : NatF Nat → Nat
In Zero = (1 , Zero)
In (Suc (n , v)) = (suc n , Suc v)

Out : Nat → NatF Nat
Out (zero , v) = Zero
Out (suc n , Zero) = Zero
Out (suc n , Suc v) = Suc (n , v)

Carrier : Set₁
Carrier = Set → Set → Set

Bifunctor : Carrier → Set₁
Bifunctor C = ∀{A A' B B' : Set} → (A → A') → (B → B') → C A B → C A' B'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set₁
FoldT Alg R = ∀{X : Carrier} → Bifunctor X → Alg X → (NatF R) → X R (NatF R) 

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           (∀ (R P : Set)
              (up : R → P)
              (sfo : FoldT SAlg R) →
              (abstIn : NatF R → P)
              (rec : R → X R R)
              (d : NatF R) → 
              X R P) → 
           SAlg X

id : ∀{X : Set} → X → X
id x = x

foldh : (n : ℕ) → FoldT SAlg (INatF n)
foldh zero bi (mkSAlg salg) v =
  salg ⊥ (NatF ⊥) (λ()) {!!} id (λ()) v
foldh (suc n) bi (mkSAlg salg) v = 
  salg {!!} {!!} {!!} {!!} {!!} {!!} v

fold : FoldT SAlg Nat 
fold bi a x with In x
fold bi a x | (suc n , v) = bi (λ v → (n , v)) {!!} (foldh n bi a v)

