open import lib

open import FunctorCodes

module Eq{n : ℕ}(F : Fnc n) where

open import Mu{n} F 

{-# NO_UNIVERSE_CHECK #-}
data Eq : Mu → Mu → Set where
  eqMu : ∀{R : Set}(r : R → Mu)(d : interp F R) → 
         (d' : interp F R) →

-- Mendler trick at kind Mu → Mu → Set:
         (U : Mu → Mu → Set) →
         (revealU : ∀{m m' : Mu} → U m m' → Eq m m') → 

{- interpEq takes a predicate 'base' to apply at the base case.  Here,
   we supply one which uses the functions r and r' from the Mendler trick
   for Mu to map from R and R' to Mu, so that the predicate U can be
   applied -}
         (ic : interpCong F (λ v v' → U (r v) (r v')) d d') →

         Eq (inMu r d) (inMu r d')

eqRefl : ∀(m : Mu) → Eq m m 
eqRefl (inMu{R} r d) =
  eqMu r d d Eq (λ p → p) (h F d)

  where h : ∀{n : ℕ}(F : Fnc n) →
             (d : interp F R) → 
             interpCong F (λ v v' → Eq (r v) (r v')) d d
        h (Sum F₁ F₂) (inj₁ x) = h F₁ x
        h (Sum F₁ F₂) (inj₂ y) = h F₂ y
        h (Prod F₁ F₂) (x₁ , x₂) = (h F₁ x₁ , h F₂ x₂)
        h (Arrow X F) d = λ x → h F (d x)
        h Param d = eqRefl (r d)
        h (Other x) d = triv

EqTrans : {m1 m2 m3 : Mu} → Eq m1 m2 → Eq m2 m3 → Eq m1 m3 
EqTrans (eqMu r d1 d2 U1 revealU1 ic1) (eqMu .r .d2 d3 U2 revealU2 ic2) =
 eqMu r d1 d3 Eq (λ x → x) (eqh{n}{F} r d1 d2 d3 ic1 ic2)
 where
   eqh : ∀{n' : ℕ}{F' : Fnc n'} → 
          {R : Set}(r : R → Mu) → 
          (d1 : interp F' R) →
          (d2 : interp F' R) →
          (d3 : interp F' R) →
          interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 →
          interpCong F' (λ v v' → U2 (r v) (r v')) d2 d3 →                  
          interpCong F' (λ v v' → Eq (r v) (r v')) d1 d3
   eqh{F' = Sum{n'}{n''} F' F''} r (inj₁ d1) (inj₁ d2) (inj₁ d3) ic1 ic2 =
       eqh{n'}{F'} r d1 d2 d3 ic1 ic2
   eqh {F' = Sum{n'}{n''} F' F''} r (inj₂ d1) (inj₂ d2) (inj₂ d3) ic1 ic2 = 
       eqh{n''}{F''} r d1 d2 d3 ic1 ic2
   eqh {F' = Prod{n'}{n''} F' F''} r (d1 , d1') (d2 , d2') (d3 , d3') (ic1 , ic1') (ic2 , ic2') =
       (eqh{n'}{F'} r d1 d2 d3 ic1 ic2 , eqh{n''}{F''} r d1' d2' d3' ic1' ic2' )
   eqh {F' = Arrow X F'} r d1 d2 d3 ic1 ic2 =
       λ x → eqh{F' = F'} r (d1 x) (d2 x) (d3 x) (ic1 x) (ic2 x)
   eqh {F' = Param} r d1 d2 d3 ic1 ic2 = EqTrans (revealU1 ic1) (revealU2 ic2)
   eqh {F' = Other X} r d1 d2 d3 ic1 ic2 = triv
