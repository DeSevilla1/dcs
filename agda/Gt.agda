open import lib
open import FunctorCodes 

module Gt{n : ℕ}(F : Fnc n) where

open import Mu{n} F 
open import Eq{n} F

{- when is a value in interp F' R equal to another value, of type Mu. -}   
eqFMu : {R : Set}(r : R → Mu)
        {n' : ℕ}(F' : Fnc n') →
        interp F' R → Mu → Set
eqFMu _ (Sum F F₁) x v = ⊥
eqFMu _ (Prod F F₁) x v = ⊥
eqFMu _ (Arrow X F) x v = ⊥
eqFMu r Param x v = Eq (r x) v
eqFMu _ (Other x₁) x v = ⊥

mutual

 geqFMu : {R : Set}(r : R → Mu){n' : ℕ}(F' : Fnc n') →
          (Mu → Mu → Set) → 
          interp F' R → Mu → Set
 geqFMu r F base x v = eqFMu r F x v ∨ gtFMu r F base x v

 {- one-step structural decrease from F Mu to Mu, using base when we
    hit a Param in the functor. -}   
 gtFMu : {R : Set}(r : R → Mu) → 
         {n' : ℕ}(F' : Fnc n') →
         (Mu → Mu → Set) → 
         interp F' R → Mu → Set
 gtFMu r (Sum F₁ F₂) base (inj₁ x) v = geqFMu r F₁ base x v 
 gtFMu r (Sum F₁ F₂) base (inj₂ y) v = geqFMu r F₂ base y v 
 gtFMu r (Prod F₁ F₂) base (x₁ , x₂) v =  geqFMu r F₁ base x₁ v 
                                        ∨ geqFMu r F₂ base x₂ v 
 gtFMu r (Arrow X F') base f v = Σ X (λ x → geqFMu r F' base (f x) v)
 gtFMu r Param base x v = base (r x) v
 gtFMu _ (Other _) _ _ _ = ⊥
 
{- when is one value of type Mu strictly greater than another -}
{-# NO_UNIVERSE_CHECK #-}
data Gt : Mu → Mu → Set where
  gt1 : {R : Set}(r : R → Mu)(d : interp F R) → 
        (v : Mu) →

        -- Mendler trick
        (U : Mu → Mu → Set) →
        (revealU : ∀{m m' : Mu} → U m m' → Gt m m') → 

        (dec : gtFMu r F U d v) →

        Gt (inMu r d) v
  gtCong : {R : Set}(r : R → Mu)(d : interp F R) → 
           (d' : interp F R) →

           -- Mendler trick
           (U : Mu → Mu → Set) →
           (revealU : ∀{m m' : Mu} → U m m' → Gt m m') → 
  
           (ic : interpCong F (λ v v' → U (r v) (r v')) d d') →
 
           {- this condition is to make sure that the above interpCong clause
              does not just hold because the Param case, when we recursively
              reference Gt (via U), is never reached -}
           (rch : reachesParam F d) → 

           Gt (inMu r d) (inMu r d')

-- m strictly greater than m'
Ge : {n : ℕ}{F : Fnc n} → Mu → Mu → Set
Ge m m' = Gt m m' ∨ Eq m m'

