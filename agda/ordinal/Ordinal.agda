open import lib

module Ordinal where

data Ord : Set where
  Z : Ord
  S : Ord → Ord
  L : (ℕ → Ord) → Ord

data Eq : Ord → Ord → Set where
  EqZ : Eq Z Z
  EqS : ∀{x y : Ord} → Eq x y → Eq (S x) (S y)
  EqL : ∀{f f' : ℕ → Ord} → (∀(n : ℕ) → Eq (f n) (f' n)) → Eq (L f) (L f')

eqRefl : ∀(m : Ord) → Eq m m
eqRefl Z = EqZ
eqRefl (S m) = EqS (eqRefl m)
eqRefl (L x) = EqL λ n → eqRefl (x n)