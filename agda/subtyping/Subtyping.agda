open import lib

module Subtyping where

data Tp : Set where
  Base : Tp
  _⇒_ : Tp → Tp → Tp

data Pol : Set where
  Pos : Pol
  Neg : Pol

flip : Pol → Pol
flip Pos = Neg
flip Neg = Pos

data TpOp : Pol → Set where
  Base : ∀{p : Pol} → TpOp p
  _⇒_ : ∀{p : Pol} → TpOp (flip p) → TpOp p → TpOp p
  Param : TpOp Pos

Functor : Set
Functor = TpOp Pos

-- for functor (X ↦ Base ⇒ X)
example : Functor
example = Base ⇒ Param

nonExample : TpOp Neg
nonExample = Param ⇒ Base

_·_ : ∀{p : Pol} → TpOp p → Tp → Tp
Base · t = Base
(T1 ⇒ T2) · t = (T1 · t) ⇒ (T2 · t)
Param · t = t

data _<:_ : Tp → Tp → Set where
  BaseRefl : Base <: Base
  Fun : ∀{T1 T2 T1' T2' : Tp} → 
         T1' <: T1 →
         T2 <: T2' →
         (T1 ⇒ T2) <: (T1' ⇒ T2')
  Fmap : ∀{T1 T2 T1' T2' : Tp}{F : Functor} →
         (T1 ⇒ T2) <: (T1' ⇒ T2') →
         (T1 ⇒ T2) <: ((F · T1') ⇒ (F · T2'))
