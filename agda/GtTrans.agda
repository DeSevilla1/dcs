{- Transitivity of Gt with Gt, Gt with Eq, and Eq with Gt -}

open import lib
open import FunctorCodes 

module GtTrans{n : ℕ}(F : Fnc n) where

open import Mu{n} F 
open import Eq{n} F 
open import Gt{n} F


mutual

 GtTrans : ∀{m1 m2 m3 : Mu} → Gt m1 m2 → Gt m2 m3 → Gt m1 m3
 GtTrans{m2 = m2}{m3} (gt1{R} r d1 m2 U1 revealU1 dec1) q =
   gt1 r d1 m3 Gt (λ x → x) (gt1h{n' = n}{F' = F} d1 dec1)
   where gt1h : ∀ {n' : ℕ}{F' : Fnc n'} → 
                  (d1 : interp F' R) →
                  gtFMu r F' U1 d1 m2 → 
                  gtFMu r F' Gt d1 m3
         gt1h' : ∀ {n' : ℕ}{F' : Fnc n'} → 
                   (d1 : interp F' R) →
                   geqFMu r F' U1 d1 m2 → 
                   gtFMu r F' Gt d1 m3
         gt1h {F' = Sum{n₁}{n₂} F₁ F₂} (inj₁ x) p = inj₂ (gt1h'{n₁}{F₁} x p)
         gt1h {F' = Sum{n₁}{n₂} F₁ F₂} (inj₂ x) p = inj₂ (gt1h'{n₂}{F₂} x p)
         gt1h {F' = Prod{n₁}{n₂} F₁ F₂} (x , y) (inj₁ p) =
                                                       inj₁ (inj₂ (gt1h'{n₁}{F₁} x p))
         gt1h {F' = Prod{n₁}{n₂} F₁ F₂} (x , y) (inj₂ p) =
                                                       inj₂ (inj₂ (gt1h'{n₂}{F₂} y p))
         gt1h {F' = Arrow{n₁} X F₁} d1 (x , p) = (x , inj₂ (gt1h'{n₁}{F₁} (d1 x) p))
         gt1h {F' = Param} d1 p = GtTrans (revealU1 p) q
         gt1h' {F' = Param} e1 (inj₁ p) = EqGtTrans p q
         gt1h'{n'}{F'} e1 (inj₂ p) = gt1h{n'}{F'} e1 p
 GtTrans (gtCong{R} r d1 d2 U1 revealU1 ic _)
         (gt1{R'} .r .d2 m3 U2 revealU2 dec) =
   gt1 r d1 m3 Gt (λ x → x) (cong-gt1 {n}{F} d1 d2 ic dec)
   where
    mutual 
     cong-gt1 : ∀{n' : ℕ}{F' : Fnc n'} → 
                (d1 : interp F' R) →
                (d2 : interp F' R) →
                interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 → 
                gtFMu r F' U2 d2 m3 → 
                gtFMu r F' Gt d1 m3
     cong-gt1 {F' = Sum F' F''} (inj₁ x1) (inj₁ x2) ic dec =
       cong-geq x1 x2 ic dec
     cong-gt1 {F' = Sum F' F''} (inj₂ y1) (inj₂ y2) ic dec =
       cong-geq y1 y2 ic dec
     cong-gt1 {F' = Prod F' F''} (x1 , y1) (x2 , y2) (ic1 , ic2) (inj₁ dece) =
       inj₁ (cong-geq x1 x2 ic1 dece)
     cong-gt1 {F' = Prod F' F''} (x1 , y1) (x2 , y2) (ic1 , ic2) (inj₂ dece) = 
       inj₂ (cong-geq y1 y2 ic2 dece)
     cong-gt1 {F' = Arrow X F'} d1 d2 ic (x , dec) =
       (x , cong-geq (d1 x) (d2 x) (ic x) dec)
     cong-gt1 {F' = Param} d1 d2 ic dec = GtTrans (revealU1 ic) (revealU2 dec) 

     cong-geq : ∀{n' : ℕ}{F' : Fnc n'} → 
                 (d1 : interp F' R) →
                 (d2 : interp F' R) →
                 interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 → 
                 geqFMu r F' U2 d2 m3 → 
                 geqFMu r F' Gt d1 m3 
     cong-geq {F' = Param} d1 d2 ic (inj₁ eq) =
       inj₂ (GtEqTrans (revealU1 ic) eq)
     cong-geq {n'}{F'} d1 d2 ic (inj₂ dec) = 
       inj₂ (cong-gt1 {n'}{F'} d1 d2 ic dec)        

 GtTrans (gtCong{R} r d1 d2 U1 revealU1 ic1 rch1)
         (gtCong .r .d2 d3 U2 revealU2 ic2 _) =
     gtCong r d1 d3 Gt (λ x → x) (cong-geq2{n}{F} d1 d2 d3 ic1 ic2) rch1
  where
     cong-geq2 : ∀{n' : ℕ}{F' : Fnc n'} → 
                 (d1 : interp F' R) →
                 (d2 : interp F' R) →
                 (d3 : interp F' R) →
                 interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 →
                 interpCong F' (λ v v' → U2 (r v) (r v')) d2 d3 →                  
                 interpCong F' (λ v v' → Gt (r v) (r v')) d1 d3
     cong-geq2{F' = Sum{n'}{n''} F' F''} (inj₁ d1) (inj₁ d2) (inj₁ d3) ic1 ic2 =
       cong-geq2{n'}{F'} d1 d2 d3 ic1 ic2
     cong-geq2 {F' = Sum{n'}{n''} F' F''} (inj₂ d1) (inj₂ d2) (inj₂ d3) ic1 ic2 = 
       cong-geq2{n''}{F''} d1 d2 d3 ic1 ic2
     cong-geq2 {F' = Prod{n'}{n''} F' F''} (d1 , d1') (d2 , d2') (d3 , d3') (ic1 , ic1') (ic2 , ic2') =
       (cong-geq2{n'}{F'} d1 d2 d3 ic1 ic2 , cong-geq2{n''}{F''} d1' d2' d3' ic1' ic2' )
     cong-geq2 {F' = Arrow X F'} d1 d2 d3 ic1 ic2 =
       λ x → cong-geq2{F' = F'} (d1 x) (d2 x) (d3 x) (ic1 x) (ic2 x)
     cong-geq2 {F' = Param} d1 d2 d3 ic1 ic2 = GtTrans (revealU1 ic1) (revealU2 ic2)
     cong-geq2 {F' = Other X} d1 d2 d3 ic1 ic2 = triv
 EqGtTrans : ∀{m1 m2 m3 : Mu} → Eq m1 m2 → Gt m2 m3 → Gt m1 m3
 EqGtTrans (eqMu{R} r d1 d2 U1 revealU1 ic) (gt1 .r .d2 m3 U2 revealU2 dec) = 
   gt1 r d1 m3 Gt (λ x → x) (eqgt1{n}{F} d1 d2 ic dec)
   where
    mutual
     eqgt1 : ∀{n' : ℕ}{F' : Fnc n'} → 
              (d1 : interp F' R) →
              (d2 : interp F' R) →
              interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 → 
              gtFMu r F' U2 d2 m3 → 
              gtFMu r F' Gt d1 m3
     eqgt1 {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) ic dece = cong-geq d1 d2 ic dece
     eqgt1 {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) ic dece = cong-geq d1 d2 ic dece
     eqgt1 {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₁ dece) = inj₁ (cong-geq d1 d2 ic dece)
     eqgt1 {F' = Prod F' F''} (d1 , d1') (d2 , d2') (ic , ic') (inj₂ dece) = inj₂ (cong-geq d1' d2' ic' dece)
     eqgt1 {F' = Arrow X F'} d1 d2 ic (x , dec) = (x , cong-geq (d1 x) (d2 x) (ic x) dec )
     eqgt1 {F' = Param} d1 d2 ic dec = EqGtTrans (revealU1 ic) (revealU2 dec)

     cong-geq : ∀{n' : ℕ}{F' : Fnc n'} → 
                 (d1 : interp F' R) →
                 (d2 : interp F' R) →
                 interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 → 
                 geqFMu r F' U2 d2 m3 → 
                 geqFMu r F' Gt d1 m3 
     cong-geq{F' = Param} d1 d2 ic (inj₁ eq) = inj₁ (EqTrans (revealU1 ic) eq)
     cong-geq{n'}{F'} d1 d2 ic (inj₂ dec) = inj₂ (eqgt1{n'}{F'} d1 d2 ic dec)
 EqGtTrans (eqMu{R} r d1 d2 U1 revealU1 ic1) (gtCong .r .d2 d3 U2 revealU2 ic2 rch) =
   gtCong r d1 d3 Gt (λ x → x) (cong-eqg{n}{F} d1 d2 d3 ic1 ic2) (reachesParamCong'{n}{F} d1 d2 ic1 rch)
   where
     cong-eqg : ∀{n' : ℕ}{F' : Fnc n'} → 
                 (d1 : interp F' R) →
                 (d2 : interp F' R) →
                 (d3 : interp F' R) →
                 interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 →
                 interpCong F' (λ v v' → U2 (r v) (r v')) d2 d3 →                  
                 interpCong F' (λ v v' → Gt (r v) (r v')) d1 d3
     cong-eqg {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) (inj₁ d3) ic1 ic2 =
       cong-eqg{F' = F'} d1 d2 d3 ic1 ic2
     cong-eqg {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) (inj₂ d3) ic1 ic2 =
       cong-eqg{F' = F''} d1 d2 d3 ic1 ic2
     cong-eqg {F' = Prod F' F''} (d1 , d1') (d2 , d2') (d3 , d3') (ic1 , ic1') (ic2 , ic2') =
       (cong-eqg{F' = F'} d1 d2 d3 ic1 ic2 , cong-eqg{F' = F''} d1' d2' d3' ic1' ic2') 
     cong-eqg {F' = Arrow X F'} d1 d2 d3 ic1 ic2 =
       λ x → cong-eqg{F' = F'} (d1 x) (d2 x) (d3 x) (ic1 x) (ic2 x) 
     cong-eqg {F' = Param} d1 d2 d3 ic1 ic2 = EqGtTrans (revealU1 ic1) (revealU2 ic2)
     cong-eqg {F' = Other X} d1 d2 d3 ic1 ic2 = triv


 GtEqTrans : ∀{m1 m2 m3 : Mu} → Gt m1 m2 → Eq m2 m3 → Gt m1 m3
 GtEqTrans{m3 = m3} (gt1{R} r1 d1 (inMu r2 d2) U1 revealU1 dec) q =
   gt1 r1 d1 m3 Gt (λ x → x) (gt1h{n}{F} d1 dec)
   where
    mutual 
     gt1h : ∀ {n' : ℕ}{F' : Fnc n'} → 
              (d1 : interp F' R) →
              gtFMu r1 F' U1 d1 (inMu r2 d2) → 
              gtFMu r1 F' Gt d1 m3
     gt1h {F' = Sum{n₁}{n₂} F₁ F₂} (inj₁ x) p = gt1h'{n₁}{F₁} x p
     gt1h {F' = Sum{n₁}{n₂} F₁ F₂} (inj₂ x) p = gt1h'{n₂}{F₂} x p
     gt1h {F' = Prod{n₁}{n₂} F₁ F₂} (x , y) (inj₁ p) = inj₁ (gt1h'{n₁}{F₁} x p)
     gt1h {F' = Prod{n₁}{n₂} F₁ F₂} (x , y) (inj₂ p) = inj₂ (gt1h'{n₂}{F₂} y p)
     gt1h {F' = Arrow{n₁} X F₁} d1 (x , p) = (x , gt1h'{n₁}{F₁} (d1 x) p)
     gt1h {F' = Param} d1 p = GtEqTrans (revealU1 p) q
 
     gt1h' : ∀ {n' : ℕ}{F' : Fnc n'} → 
               (d1 : interp F' R) →
               geqFMu r1 F' U1 d1 (inMu r2 d2) → 
               geqFMu r1 F' Gt d1 m3
     gt1h' {F' = Param} e1 (inj₁ p) = inj₁ (EqTrans p q)
     gt1h'{n'}{F'} e1 (inj₂ p) = inj₂ (gt1h{n'}{F'} e1 p)

 GtEqTrans (gtCong{R} r d1 d2 U1 revealU1 ic1 rch) (eqMu .r .d2 d3 U2 revealU2 ic2) =
  gtCong r d1 d3 Gt (λ x → x) (cong-eqg{n}{F} d1 d2 d3 ic1 ic2) rch
  where
     cong-eqg : ∀{n' : ℕ}{F' : Fnc n'} → 
                 (d1 : interp F' R) →
                 (d2 : interp F' R) →
                 (d3 : interp F' R) →
                 interpCong F' (λ v v' → U1 (r v) (r v')) d1 d2 →
                 interpCong F' (λ v v' → U2 (r v) (r v')) d2 d3 →                  
                 interpCong F' (λ v v' → Gt (r v) (r v')) d1 d3
     cong-eqg {F' = Sum F' F''} (inj₁ d1) (inj₁ d2) (inj₁ d3) ic1 ic2 =
       cong-eqg{F' = F'} d1 d2 d3 ic1 ic2
     cong-eqg {F' = Sum F' F''} (inj₂ d1) (inj₂ d2) (inj₂ d3) ic1 ic2 =
       cong-eqg{F' = F''} d1 d2 d3 ic1 ic2
     cong-eqg {F' = Prod F' F''} (d1 , d1') (d2 , d2') (d3 , d3') (ic1 , ic1') (ic2 , ic2') =
       (cong-eqg{F' = F'} d1 d2 d3 ic1 ic2 , cong-eqg{F' = F''} d1' d2' d3' ic1' ic2') 
     cong-eqg {F' = Arrow X F'} d1 d2 d3 ic1 ic2 =
       λ x → cong-eqg{F' = F'} (d1 x) (d2 x) (d3 x) (ic1 x) (ic2 x) 
     cong-eqg {F' = Param} d1 d2 d3 ic1 ic2 = GtEqTrans (revealU1 ic1) (revealU2 ic2)
     cong-eqg {F' = Other X} d1 d2 d3 ic1 ic2 = triv

