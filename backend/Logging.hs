module Logging where

import Prelude hiding (log)
import System.IO
import Util

logInit :: FilePath -> IO Handle
logInit f =
  do
    h <- openFile f WriteMode
    hSetBuffering h NoBuffering
    return h

log :: Handle -> String -> a -> a
log h msg = safePerformIO (hPutStr h (msg ++ "\n"))

logIf :: Bool -> Handle -> String -> a -> a
logIf True = log
logIf False = \ _ _ -> id