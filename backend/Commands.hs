module Commands where

import Prelude hiding (lookup)
import Pos
import Arities
import Comments
import Parser
import Lexer
import Syntax
import SyntaxHelpers
import Highlighting
import ScopeMap
import UndefinedSyms 
import Variances
import VariancesMap
import TermExtents
import Infer
import SubstTy
import Renaming
import Imports
import Util
import Logging
import Data.Char
import Data.List
import Data.Tree
import Data.Text(Text,unpack,pack)
import qualified Data.Map as M
import System.Directory
import Control.Monad.State.Lazy

{- we respond to commands from the frontend,
   initializing and then querying a DcsState. -}
data DcsState =
    DcsRoots RootList
  | DcsState
      RootList
      String {- name of file Dcs is handling presently -}
      ImportPaths {- mapping from extents of current Dcs file to canonical paths imported there -}
      SimpleFile {- parsed source file -}
      ScopeMap {- map positions of symbol occurrences to positions of corresponding binding occurrences -}
      VariancesMap {- map positions of binding occurrences of type constants to their variances -}
      VarianceMap {- map type variables to their variances -}
      TermExtentsMaps {- map starting positions to list of ending positions of subterms, and
                         similarly for ending positions to list of starting positions (two separate maps) -}
      InferState {- computed by type inference -}

initDcsState = DcsRoots [] -- initially, no roots are known

type Filename = String

cmdDelimiter :: Char
cmdDelimiter = '$'

readUntilCmdDelim :: IO String
readUntilCmdDelim = do
  c <- getChar
  if c == cmdDelimiter then
    return []
  else
    do
      (c : ) <$> readUntilCmdDelim

readCmdAndArgs :: IO (String,String)
readCmdAndArgs = 
  do
    l <- readUntilCmdDelim
    let (cmd,(_:args)) = break (== ' ') (dropWhile isSpace l) 
    return (cmd,args)

startReply :: IO ()
startReply =
  putStr "(progn "

addReply :: [String] -> IO ()
addReply ss =
  mapM_ putStr ss

completeReply :: [String] -> IO ()
completeReply ss =
  do
    addReply ss
    finishReply

finishReply :: IO ()
finishReply =
  do
    putStr ")"
    putStr [cmdDelimiter]

sendReply :: [String] -> IO ()
sendReply ss =
  do
    startReply
    addReply ss
    finishReply

showParsingError :: Either String String -> String
showParsingError m =
  let (s,e) = (case m of
                 Left e -> ("lexing",e)
                 Right e -> ("parsing",e))
  in
    "(dcs-mode-parsing-error \"" ++ s ++ "\" " ++ e ++ ")" 

finishInitReply :: IO ()
finishInitReply = completeReply $ ["(dcs-mode-finish-init)"]

processFile :: RootList -> String -> IO DcsState
processFile roots filename = 
  do
    (comments, importPaths, result) <- gatherFiles roots filename

    startReply

    addReply $ showComment <$> comments
    case result of

      --------------------------------------------------
      -- initial error cases
      --------------------------------------------------
      Left pierr ->
        case pierr of
          Left m -> 
            do
              addReply [showParsingError m]
              finishInitReply
              return (DcsRoots roots)
          Right imperrs -> 
            do
              addReply $ showImportError <$> imperrs
              finishInitReply
              return (DcsRoots roots)

      --------------------------------------------------
      -- we were able to parse the file, and no import errors
      --------------------------------------------------
      Right fd@(deps,mainfile) ->
           do

            let (imps,depfiles) = unzip (rootLabel <$> deps)
    -- 3. send highlighting information for keywords

            addReply (showHighlighting "keyword" (keywordFile (imps,mainfile) []))
            let sfile = join $ (depfiles ++ [mainfile]) -- we will drop sfile soon...

            addReply ["(dcs-mode-comment \"end of keywords\")"]

    -- 4. compute the ScopeMap used to answer "jump" queries (handleCmd).
    --    This is also used throughout the code to map positions of variable
    --    occurrences to the positions of the corresponding binding occurrences.

            let sm = scopeMap fd

    -- 5. compute a couple maps related to variances
          
            let (varerrs,(vsm,vm)) = variances sm fd

    -- 6. send info on undefined symbols                

            addReply $ showUndefineds sm fd

    -- 7. send info on variance errors                

            addReply $ map showVarianceError varerrs

    -- 8. check arities of type expressions
    
            addReply (showArityError <$> arities sm vsm sfile)

    -- 9. get the mapping for term extents (so the user can select regions of text following syntactic structure)

            let texs = termExtents mainfile

    -- 10. infer types

            hLog <- logInit ".dcs-log-file.txt"

            let (tyerrs,infst) = infer hLog sm vsm fd

    -- 11. show any type errors

            addReply $ map showTypeError $ tyerrs

    -- 12. include an elisp call so the frontend can show errors if any

            finishInitReply

            return $ DcsState roots filename importPaths mainfile sm vsm vm texs infst

readPos :: String -> String -> Pos
readPos filename posstr = 
  let pos = read posstr :: Int in
    mkPos filename pos

{- try to get the introducing Var for a symbol, using the ScopeMap.
   In the positive case invoke the first given action with the position in posstr, and
   then that Var. In the negative case, call the second action with just the Pos. -}
getSymIntro :: Filename -> ScopeMap -> String ->
               (Pos -> Var -> IO ()) ->
               (Pos -> IO ()) ->
               IO ()
getSymIntro filename sm posstr act1 act2 = 
  let pos = readPos filename posstr in
    do
      case scopeVar pos sm of
        Just v -> act1 pos v
        Nothing -> act2 pos 

-- if the position (given by the String args) is in a variable
-- occurrence, generate code to jump there.
-- If it is in a valid import, generate code to have DCS open that import and jump there
jumpToIntro :: Filename -> ScopeMap -> ImportPaths -> String -> IO ()
jumpToIntro filename sm importPaths args =
  getSymIntro filename sm args
    (\ _ var -> sendReply ["(dcs-mode-jump-to-pos-in-file " ++ elispFullPos (varPos var) ++ ")"])
    (\ pos ->
       case M.lookupLE pos importPaths of
         Nothing -> sendReply ["(message \"Nothing to jump to at point.\")"]
         Just (_,path) -> sendReply ["(dcs-mode-jump-to-pos-in-file (cons \"" ++ path ++ "\" 1))"])
       

symInfo :: Filename -> ScopeMap -> VariancesMap -> VarianceMap -> InferState -> String -> IO ()
symInfo filename sm vsm vm infst args =
  getSymIntro filename sm args
    (\ pos var -> 
       let pos' = varPos var in
         case variance vm pos' of
           Just v -> sendReply $ [showVariance v]
           Nothing ->
             case paramVariances vsm pos' of
               Just vs -> sendReply $ [showParamVariances vs]
               Nothing ->
                 case M.lookup pos' (tmCtxt infst) of
                   Just (params,ty) ->
                     -- since ext could be inside a term somewhere, and it is not too convenient
                     -- to apply the substitution (found by solving) to all recorded types inside
                     -- a term, we apply it here
                     sendReply $ [elispTyping sm (renamingPer infst) pos var params (substTyMeta (subst infst) ty) ]
                   Nothing -> sendReply ["(message \"Nothing\")"])
    (\ _ -> sendReply ["(message \"No defined symbol at or before point\")"])


typeInfo :: String -> ScopeMap -> InferState -> String -> IO ()
typeInfo filename sm infst args =
  let [spstr,epstr] = words args 
      sp = read spstr :: Int
      ep = read epstr :: Int
      h = mkPos filename
      ext = (h sp,h ep) in
    sendReply [inspectTy sm infst ext]

{- try to get the ending positions of extents that start at the given position -}
getTermExtents :: String -> TermExtentsMaps -> String -> IO ()
getTermExtents filename (tex1,tex2) args =
  let [posstr,whichstr] = words args 
      pos = readPos filename posstr
      which = read whichstr :: Bool
  in
    do
      case M.lookupLE pos (if which then tex1 else tex2) of
        Nothing -> sendReply ["(message \"No term at or before point\")"]
        Just (pos',eps) ->
          sendReply ["(dcs-mode-set-current-extents " ++ elispPos pos' ++ " (list "
                     ++ intercalate " " (map elispPos eps) ++ ")"
                     ++ " \"" ++ show which ++ "\")"]

rehighlight :: String -> SimpleFile -> String -> IO ()
rehighlight filename sfile args =
  do
    let [posstr,callback] = words args 
        pos = readPos filename posstr
        es = (\ s -> (extentStatement s,s)) <$> sfile
    case find (\ ((sp,ep),s) -> sp <= pos && pos <= ep) es of
      Nothing -> sendReply ["(dcs-mode-noop)"]
      Just (_,s) -> 
        do
         startReply
         addReply (showHighlighting "keyword" (keywordStatement s []))
         completeReply [callback]

-- dump info to console (not intended for emacs)
dumpInferenceInfo :: InferState -> IO ()
dumpInferenceInfo infst =
  do
    putStr $ showRenamingPer (renamingPer infst)
    putStrLn $ "=========================================="
    putStr $ "Solution from constraint solver:"
    putStr $ showMetaTySubst (subst infst)
    putStrLn ""

{- the given DcsState is the one we are currently operating on.
   The frontend will not issue other commands before 'init' -}
handleCmd :: DcsState -> IO DcsState
handleCmd s@(DcsRoots rs) =
  do
    (cmd,args) <- readCmdAndArgs
    case cmd of
      "init" -> processFile rs args
      "add-root" ->
        do
          (errs,rs') <- addRoot rs args
          if null errs then
            sendReply ["(dcs-mode-noop)"] >>
            (return $ DcsRoots rs')
          else
            sendReply (showRootError <$> errs) >>
            (return $ DcsRoots rs)
      _ ->
        do
          sendReply ["(message \"Could not process command " ++ cmd ++ "\")"]
          return s

handleCmd state@(DcsState roots filename importPaths sfile sm vsm vm texs infst) = 
  do
    (cmd,args) <- readCmdAndArgs
    case cmd of
      "init" -> processFile roots args
      "info" -> symInfo filename sm vsm vm infst args >> return state
      "type-info" -> typeInfo filename sm infst args >> return state
      "jump" -> jumpToIntro filename sm importPaths args >> return state
      "extents" -> getTermExtents filename texs args >> return state
      "rehighlight" -> rehighlight filename sfile args >> return state
      "dump" -> dumpInferenceInfo infst >> return state
      "add-root" ->
        sendReply ["(message \"Roots should be added (in your .emacs file) before loading dcs files\")"] >>
           return state
      _ -> sendReply ["(message \"unrecognized command " ++ cmd ++ "\")"] >>
           return state
