module UndefinedSyms where

import Pos
import ScopeMap
import Syntax
import SyntaxHelpers
import Control.Monad
import Control.Applicative hiding (Const)
import Control.Monad.Reader
import Data.Monoid
import qualified Data.Map as M
import Util
import Imports

data UndefError =
   UndefinedSymbol Var
 | UndefinedSymbolsInImport Extent 

startUndefError :: Extent -> String
startUndefError (sp,ep) = "(dcs-mode-undefined-sym " ++ elispPos sp ++ " " ++ elispPos ep ++ " \""

showUndefError :: UndefError -> String
showUndefError (UndefinedSymbol v) = 
  startUndefError (mkExtent (varPos v) (endingPosVar v)) ++ varStr v ++ "\")"
showUndefError (UndefinedSymbolsInImport e) =
  startUndefError e ++ "Undefined symbols in imported file or its imports\")"
   
showUndefineds :: ScopeMap -> FileWithDeps -> [String]
showUndefineds sm fd =
  map showUndefError      
  (runReader (undefFileWithDeps fd) sm)

-- compute a list of Vars not in the domain of ScopeMap
type UndefReader = Reader ScopeMap [UndefError]
type Undef a = a -> UndefReader

undefMaybe :: Undef a -> Undef (Maybe a)
undefMaybe u Nothing = return []
undefMaybe u (Just x) = u x

undefFileWithDeps :: Undef FileWithDeps
undefFileWithDeps = processFileWithDeps undefSimpleFile UndefinedSymbolsInImport 

undefSimpleFile :: Undef SimpleFile
undefSimpleFile f = join <$> mapM undefStatement f

undefStatement :: Undef Statement
undefStatement (TyDefSt (TyDef _ _ _ ty)) = undefTy ty
undefStatement (DataDefSt (DataDef _ _ _ ctors)) = join <$> mapM undefTApp ctors
undefStatement (TmDefSt (TmDef _ _ _ mty t)) =
  aseq [undefMaybe undefTy mty, undefTm t]

undefVar :: Undef Var
undefVar v = 
  do
    sm <- ask
    return $ if varStr v == unusedVar || not (boundVar (varPos v) sm) then [UndefinedSymbol v] else []

undefTApp :: Undef TApp
undefTApp (v,tys) = aseq [ undefVar v, join <$> mapM undefTy tys]

undefTy :: Undef Ty
undefTy (TAppMeta _) = return []
undefTy (Arrow _ _ t1 t2) = aseq [undefTy t1 , undefTy t2 ]
undefTy (TApp ta) = undefTApp ta 
undefTy (TyParens _ c) = undefTy c

undefTm :: Undef Tm
undefTm (Var v) = undefVar v
undefTm (Lam _ _ t) = undefTm t
undefTm (App t1 t2) = aseq [undefTm t1, undefTm t2]
undefTm (Omega _ _ _ ta t) = aseq [ undefTApp ta , undefTm t]
undefTm (Gamma _ t cs) = aseq [ undefTm t , join <$> mapM undefCase cs]
undefTm (Sigma _ bs t) = aseq [ join <$> mapM undefSigmaBinding bs , undefTm t ]
undefTm (Parens _ t) = undefTm t

undefCase :: Undef Case
undefCase (Case (Pat c _) t) = aseq [undefVar c, undefTm t]

undefSigmaBinding :: Undef SigmaBinding
undefSigmaBinding (SigmaBinding _ x t) = aseq [undefVar x, undefTm t]

