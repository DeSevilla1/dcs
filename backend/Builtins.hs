{-# LANGUAGE TupleSections #-}

{- names and data structures for built-in constants -}
module Builtins where

import Pos
import PosMap
import Syntax
import SyntaxHelpers
import System.FilePath
import Trie
import Variance
import VariancesMap
import TmCtxt
import qualified Data.Map as M
import GHC.Base (eqString)

newtype BuiltinTyInfo = BuiltinTyInfo { builtinTyVariances :: [Variance] }
newtype BuiltinTmInfo = BuiltinTmInfo { builtinType :: ([TyParamTm],Ty) }

builtinsFilename :: FilePath
builtinsFilename = "α"

-- TODO: isBuiltinsPos

builtinPos :: Int -> Pos
builtinPos = mkPos builtinsFilename

builtinName :: Int -> String -> Var
builtinName n v = (v,builtinPos n)

builtinEntry :: (Int, (String, a)) -> (Var,a)
builtinEntry (n, (v, i)) = (builtinName n v, i)

toDeclList :: Int -> [(String,a)] -> [(Var,a)]
toDeclList n ds = builtinEntry <$> zip [n..] ds

builtinTyDecls :: [(Var,BuiltinTyInfo)]
builtinTyDecls =
  toDeclList 1
  [("Gen", BuiltinTyInfo [Positive]) ]

cGen :: Const
cGen = fst (head builtinTyDecls)

builtinTmDecls :: [(Var,BuiltinTmInfo)]
builtinTmDecls =
  let cx = ("X", builtinPos 1)
      cy = ("Y", builtinPos 2)
      cd = ("D", builtinPos 3)
      cr = ("R", builtinPos 4)
      cc = ("C", builtinPos 5)
      tyx = tyConst cx
      tyy = tyConst cy
      arrxy = arrowTy [tyx] tyy
      tyd = tyConst cd
      tyr = tyConst cr
      tyc = tyConst cc
  in
    toDeclList (1 + length builtinTyDecls)
    [("fix", BuiltinTmInfo ([TyParam cx, TyParam cy],
                             TApp (cGen, [arrowTy [arrxy] arrxy]))),
     -- like/out D (R ~ D) : R -> D
     ("like/out", BuiltinTmInfo ([TyParam cd, TyParamLike [] cr tyd],
                                  arrowTy [tyr] tyd)),
     -- like/sig D (R ~ D) : R -> D R
     ("like/sig", BuiltinTmInfo ([TyParam cd, TyParamLike [] cr tyd],
                                  Arrow [] ArrowPlain tyr (TApp (cd, [tyr])))),
     -- like/alg C D (R ~ D) : (D => C) -> (R -> C R R)
     ("like/alg", BuiltinTmInfo ([TyParam cc, TyParam cd, TyParamLike [] cr tyd],
                                  Arrow [] ArrowPlain (Arrow [] ArrowFat tyd tyc)
                                    (Arrow [] ArrowPlain tyr (TApp (cc, [tyr, tyr])))))]

builtinPosMap :: [(Var,a)] -> PosMap
builtinPosMap ds = fromList ((\ (v,_) -> (varStr v, v)) <$> ds)

builtinCtxt :: Context
builtinCtxt = (builtinPosMap builtinTmDecls, builtinPosMap builtinTyDecls)

builtinTypeVariances :: VariancesMap
builtinTypeVariances =
  M.fromList ((\ (v,BuiltinTyInfo vs {- variances -}) -> (varPos v, vs)) <$> builtinTyDecls)

builtinTmCtxt :: TmCtxt
builtinTmCtxt = M.fromList ((\ (v,BuiltinTmInfo pty) -> (varPos v, pty)) <$> builtinTmDecls)

{- omega-terms introduce some variables in their bodies.  These helpers
   return those variables, whose positions have to be chosen with care.
   The f is the name for recursion in the omega-term -}
omegaVarRP :: Var -> Int -> Var
omegaVarRP f i = (varStr f ++ "/R_P", builtinPos i)

omegaVarFRP :: Var -> Int -> Var
omegaVarFRP f i = (varStr f ++ "/FR_P", builtinPos i)