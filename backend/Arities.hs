module Arities where

import Syntax
import SyntaxHelpers
import Pos
import ScopeMap
import Util
import Variance
import VariancesMap
import qualified Data.Map as M
import Control.Monad.Reader
import Control.Monad.State.Lazy
import Data.List

type Arity = Int

data TypeWhich = DatatypeWhich | PlainTypeWhich
  deriving Eq 

instance Show TypeWhich where
  show DatatypeWhich = "datatype"
  show PlainTypeWhich = "defined type"

type ArityMap = M.Map Pos (TypeWhich,Arity)

data Kind = TypeK | FunctorK | BifunctorK
  deriving Eq 

arityArrowParts :: Arrow -> (Kind,Kind)
arityArrowParts ArrowPlain = (TypeK,TypeK)
arityArrowParts ArrowFat = (FunctorK,BifunctorK)

-- how many type arguments will something of the given Kind need to be a type?
neededTypeArgs :: Kind -> Int
neededTypeArgs TypeK = 0
neededTypeArgs FunctorK = 1
neededTypeArgs BifunctorK = 2

data ArityError =
   ArityMismatch Extent {- where is the error -}
                 Kind {- expected kind -}
                 TypeWhich {- what sort of constant did we find? -}
                 Arity {- actual -}
 | WrongVariance Extent Const Variance {- actual -}
 | WrongKind Extent Kind {- expected -} Kind {- actual -}
 | ArityErrorStar1 Extent
 | ArityErrorStar2 Extent
 | ArityErrorStar3 Extent 

instance Show Kind where
  show BifunctorK = "bifunctor"  
  show FunctorK = "functor"
  show TypeK = "type"  

startArityError :: Extent -> String
startArityError (sp,ep) = "(dcs-mode-arity-error " ++ elispPos sp ++ " " ++ elispPos ep

showArityError :: ArityError -> String
showArityError (ArityMismatch ext k w a) =
  startArityError ext ++
  " \"  Expecting a " ++ show k ++ ", found arity " ++ show a ++
  " (" ++ show w ++ ")\\n\")"
showArityError (WrongKind ext k k') =
  startArityError ext ++
  " \"  Expected a " ++ show k ++ ", found a " ++ show k' ++ "\\n\")"
showArityError (WrongVariance ext c v) =
  startArityError ext ++
  " \"  Expected a functor, but the last type parameter of " ++ (constStr c) ++ " has variance " ++ show v ++ "\\n\")"
showArityError (ArityErrorStar1 ext) =
  startArityError ext ++
  " \"  * is being used as a functor, but it is only allowed as a type.\\n\")"
showArityError (ArityErrorStar2 ext) =
  startArityError ext ++
  " \"  * is being applied to arguments, which is not allowed.\\n\")"
showArityError (ArityErrorStar3 ext) =
  startArityError ext ++
  " \"  * is being used in a type definition, but it is only allowed in a data or func definition.\\n\")"

type AritiesReader = Reader ArityMap [ArityError]
type Arities a = ScopeMap -> VariancesMap -> a -> AritiesReader

arities :: ScopeMap -> VariancesMap -> SimpleFile -> [ArityError]
arities sm vsm f = runReader (aritiesSimpleFile sm vsm f) M.empty

aritiesSimpleFile :: Arities SimpleFile
aritiesSimpleFile sm vsm [] = return []
aritiesSimpleFile sm vsm (s:f) = aritiesStatement (aritiesSimpleFile sm vsm f) sm vsm s 

addArityMapping :: Const -> (TypeWhich,Arity) -> AritiesReader -> AritiesReader
addArityMapping c v r = local (M.insert (constPos c) v) r

-- add the given type parameters to the context around Reader r
addTypeParams :: AritiesReader -> Arities [Var]
addTypeParams r sm vsm vs = foldr (\ v -> local (M.insert (varPos v) (PlainTypeWhich,0))) r vs

addTypeParamsTm :: AritiesReader -> Arities [TyParamTm]
addTypeParamsTm r sm vsm [] = r
addTypeParamsTm r sm vsm (TyParam v : params) = addTypeParamTm r v sm vsm params
addTypeParamsTm r sm vsm (TyParamLike _ v ty : params) =
  aseq [ aritiesTy TypeK sm vsm ty , addTypeParamTm r v sm vsm params]

addTypeParamTm :: AritiesReader -> Var -> Arities [TyParamTm]
addTypeParamTm r v sm vsm params =
  local (M.insert (varPos v) (PlainTypeWhich,0)) $
  addTypeParamsTm r sm vsm params 

aritiesStatement :: AritiesReader -> Arities Statement
aritiesStatement r sm vsm (TyDefSt (TyDef _ c params ty)) =
  let v = (PlainTypeWhich,length params)
  in
    aseq [ addTypeParams (aritiesTy TypeK sm vsm ty) sm vsm params, 
           addArityMapping c v r]
aritiesStatement r sm vsm (DataDefSt (DataDef _ c params cs)) =
  let v = (DatatypeWhich, length params) in
    aseq [ addArityMapping c v r,
           addTypeParams (concatMapM (aritiesTy TypeK sm vsm) (typesFromCtrs cs)) sm vsm params ]
aritiesStatement r sm vsm (TmDefSt (TmDef _ _ params mty t)) =
  aseq $ (map (\ u -> addTypeParamsTm u sm vsm params)
            [ maybeMapM (aritiesTy TypeK sm vsm) mty,
              aritiesTm sm vsm t]
          ++ [r])

-- check that the last n Variances of vs are okFunctorVariances, assuming vs's length is at least n.
-- The Const c is the one to mention if there is a wrong variance error
checkFunctoriality :: Const -> Int -> [Variance] -> [ArityError]
checkFunctoriality c n vs =
  (WrongVariance (extentConst c) c) <$> (filter (not . okFunctorVariance) $ drop (length vs - n) vs)

{- Check that applying the given constant to Arity a type arguments
   will result in an expression of Kind k. -}
aritiesConst :: Kind -> Arity -> Arities Const
aritiesConst k a sm vsm c =
  do
    let p = constPos (bindingIf sm c)
        n = neededTypeArgs k
    am <- ask
    case (M.lookup p am, M.lookup p vsm) of
      (Just (w,a'), Just vs) ->
        let opts = (case w of { DatatypeWhich -> [a' - n,a' - n + 1] ; _ -> [a' - n]}) in
          if not (a `elem` opts) then
            -- num args not ok
            return [ArityMismatch (extentConst c) k w (a' - a)]
          else
            return (checkFunctoriality c n vs) 
      _ -> return [] -- should happen only if undefined variable (reported elsewhere)


-- the first argument is for what kind of statement we are checking outside of this call
aritiesTy :: Kind -> Arities Ty
aritiesTy _ sm vsm (TAppMeta _) = return []
aritiesTy k sm vsm ty@(Arrow _ arr t1 t2) =
  let (k1,k2) = arityArrowParts arr in
    aseq ((if (k /= TypeK) then (return [WrongKind (extentTy ty) TypeK k]) else return []) :
          [aritiesTy k1 sm vsm t1 , aritiesTy k2 sm vsm t2])
aritiesTy k sm vsm (TApp ta) = aritiesTApp k sm vsm ta
aritiesTy k sm vsm (TyParens _ t) = aritiesTy k sm vsm t

aritiesTApp :: Kind -> Arities TApp
aritiesTApp k sm vsm (c,tys) =
  aseq [ aritiesConst k (length tys) sm vsm c , concatMapM (aritiesTy TypeK sm vsm) tys ]

aritiesTm :: Arities Tm
aritiesTm sm vsm (Var _) = return []
aritiesTm sm vsm (Lam _ _ t) = aritiesTm sm vsm t
aritiesTm sm vsm (App t1 t2) = aseq $ map (aritiesTm sm vsm) [ t1 , t2 ]
aritiesTm sm vsm (Omega _ _ _ ta t) = aseq [ aritiesTApp BifunctorK sm vsm ta ,
                                         aritiesTm sm vsm t]
aritiesTm sm vsm (Gamma _ t cs) = aseq [ aritiesTm sm vsm t , concatMapM (aritiesCase sm vsm) cs]
aritiesTm sm vsm (Sigma _ bs t) = aseq [ concatMapM (aritiesSigmaBinding sm vsm) bs, aritiesTm sm vsm t ]
aritiesTm sm vsm (Parens _ t) = aritiesTm sm vsm t

aritiesCase :: Arities Case
aritiesCase sm vsm (Case _ t) = aritiesTm sm vsm t

aritiesSigmaBinding :: Arities SigmaBinding
aritiesSigmaBinding sm vsm (SigmaBinding _ _ t) = aritiesTm sm vsm t