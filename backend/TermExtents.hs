{- compute all the extents (starting position and ending position) for the terms occuring in a File -}
module TermExtents where

import Syntax
import SyntaxHelpers
import Pos
import Util
import qualified Data.Map as M
import Control.Monad.State.Lazy

type TermExtentsMap = M.Map Pos [Pos]

{- a pair of mappings, where

   - the first maps s to [e1,...,ek] if extents (s,e1),...,(s,ek) are all those with starting position s, and
   - the first maps e to [s1,...,sk] if extents (s1,e),...,(sk,e) are all those with ending position e -}
type TermExtentsMaps = (TermExtentsMap,TermExtentsMap)

type TermExtents a r = a -> State TermExtentsMaps r

initTermExtentsMaps :: TermExtentsMaps 
initTermExtentsMaps = (M.empty,M.empty)

termExtentsImport :: TermExtents Import ()
termExtentsImport i = addExtent (extentImport i) >> return ()

termExtents :: SimpleFile -> TermExtentsMaps
termExtents s = execState (termExtentsSimpleFile s) initTermExtentsMaps

termExtentsSimpleFile :: TermExtents SimpleFile ()
termExtentsSimpleFile f = mapM_ termExtentsStatement f

termExtentsStatement :: TermExtents Statement ()
termExtentsStatement (DataDefSt (DataDef _ c _ _)) = addExtent (extentConst c) >> return ()
termExtentsStatement (TyDefSt _) = return ()
termExtentsStatement (TmDefSt d@(TmDef _ c _ _ t)) =
  addExtent (extentConst c) >>
  termExtentsTm t >>
  return ()

addExtent :: Extent -> State TermExtentsMaps Extent
addExtent (sp,ep) =
  do
    (m1,m2) <- get
    put $ (M.insertWith (++) sp [ep] m1,
           M.insertWith (++) ep [sp] m2)
    return (sp,ep)

{- for efficiency, we only use the functions extentTm and startingPosTm from Syntax.hs
   when we know they will only require O(1) time.  Otherwise, we use the Extents returned
   by recursive calls, to compute the Extent for this call. -}
termExtentsTm :: TermExtents Tm Extent
termExtentsTm (Var v) = addExtent $ extentVar v 
termExtentsTm t@(Lam _ _ body) =
  do
    let sp = startingPosTm t
    (_,ep) <- termExtentsTm body
    addExtent (sp,ep)
termExtentsTm (App t1 t2) =
  do
    (sp,_) <- termExtentsTm t1
    (_,ep) <- termExtentsTm t2
    addExtent (sp,ep)
termExtentsTm t@(Omega _ f x _ body) =
  do
    let sp = startingPosTm t
    addExtent (extentVar f)
    addExtent (extentVar x)
    (_,ep) <- termExtentsTm body
    addExtent (sp,ep)
termExtentsTm t@(Gamma _ t' cs) =
  do
    termExtentsTm t'
    when (not $ null cs)
      (mapM_ termExtentsCase cs)
    addExtent $ extentTm t
termExtentsTm t@(Sigma _ bs t') =
  do
    termExtentsTm t'
    mapM_ termExtentsSigmaBinding bs
    addExtent $ extentTm t
termExtentsTm t@(Parens _ t') =
  do
    termExtentsTm t'
    addExtent $ extentTm t

termExtentsSigmaBinding :: TermExtents SigmaBinding Extent
termExtentsSigmaBinding (SigmaBinding _ x t) =
  let (sx,_) = extentVar x in
    do
      (_,et) <- termExtentsTm t
      addExtent (sx,et)

termExtentsCase :: TermExtents Case Extent
termExtentsCase (Case p t) =
  do
    (sp,_) <- termExtentsPat p
    (_,ep) <- termExtentsTm t
    addExtent (sp,ep) 

termExtentsPat :: TermExtents Pat Extent
termExtentsPat (Pat c vs) =
  do
    let (spc,epc) = extentConst c
    if (null vs) then
      addExtent (spc,epc)
    else
      do
        (_,ep) <- last <$> (mapM (addExtent . extentVar) vs)
        addExtent (spc, ep)
