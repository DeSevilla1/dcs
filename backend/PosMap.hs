module PosMap where

import Pos
import Trie
import Syntax

----------------------------------------------------------------------
-- a PosMap maps Strings to Vars
type PosMap = Trie Char Var
-- for maps from variable names to Vars for their introduction 
type Context = (PosMap,PosMap) -- the first for term-level, the second for type-level

data WhichVar = TmWhich | TyWhich

selectPosMap :: WhichVar -> Context -> PosMap
selectPosMap TmWhich = fst
selectPosMap TyWhich = snd
