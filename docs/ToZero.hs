module ProduceZero where

data Nat = Zero | Succ Nat
 deriving Show

toZero :: Nat -> Nat
toZero Zero = Zero
toZero (Succ x) = toZero x

half :: Nat -> Nat
half Zero = Zero
half (Succ Zero) = Zero
half (Succ (Succ x)) = Succ (half x)

toZeroHalf :: Nat -> Nat
toZeroHalf Zero = Zero
toZeroHalf (Succ x) = toZeroHalf (half x)

iter :: (Nat -> Nat) -> Nat -> Nat -> Nat
iter f a Zero = a
iter f a (Succ x) = f (iter f a x)

toZeroIterId :: Nat -> Nat
toZeroIterId Zero = Zero
toZeroIterId (Succ x) = toZeroIterId (iter id x x)