data Bad = Bad (Bad -> Bad)

bad :: Bad -> Bad
bad (Bad f) = f (Bad f)

loop = bad (Bad bad)

forceTheLoop = case loop of { Bad _ -> 1 }