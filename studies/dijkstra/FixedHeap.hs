{- heaps with Okasaki's algorithm to build a heap
   from a list of length n in O(n) time. -}

module FixedHeap where

data Heap a = Leaf | Node a (Heap a) (Heap a)
  deriving Show

replaceTop :: a -> Heap a -> Heap a
replaceTop x Leaf = Leaf
replaceTop x (Node y l r) = Node x l r

-- push the top value of a Node down as needed to ensure ordering property of the heap
adjust :: Ord a => Heap a -> Heap a
adjust Leaf = Leaf
adjust t@(Node x Leaf Leaf) = t
adjust (Node x l@Leaf r@(Node y l1 r1)) | x <= y = Node x l r
                                        | otherwise = Node y l (adjust (replaceTop x r))
adjust (Node x l@(Node y l1 r1) r@Leaf) | x <= y = Node x l r
                                        | otherwise = Node y (adjust (replaceTop x l )) r
adjust (Node x l@(Node y1 l1 r1) r@(Node y2 l2 r2)) | x <= y1 && x <= y2 = Node x l r
                                                    | y1 <= y2 = Node y1 (adjust (replaceTop x l)) r
                                                    | otherwise = Node y2 l (adjust (replaceTop x r))

adjustTop :: Ord a => a -> Heap a -> Heap a
adjustTop x = adjust . replaceTop x

mkNodeAdjust :: Ord a => a -> Heap a -> Heap a -> Heap a
mkNodeAdjust x l r = adjust $ Node x l r

{- from Okasaki's "Three Algorithms on Braun Trees", JFP -}
type Row a = (Int,[a])

rows :: Int -> [a] -> [Row a]
rows k [] = []
rows k xs =
  let (cur,next) = splitAt k xs in
    (k,cur) : rows (2 * k) next

{- padIf k xs d returns xs padded out on the right with enough copies of d
   so that the result has length at least k -}
padIf :: Int -> [a] -> a -> [a]
padIf 0 xs _ = xs
padIf k (h:ys) d = h : padIf (k-1) ys d
padIf k [] d = d : padIf (k-1) [] d

build :: Ord a => Row a -> [Heap a] -> [Heap a]
build (k,xs) ts = 
  let (ts1,ts2) = splitAt k ts in
    zipWith3 mkNodeAdjust xs (padIf k ts1 Leaf) (padIf k ts2 Leaf)
  
fromList :: Ord a => [a] -> Heap a
fromList xs =
  case foldr build [Leaf] (rows 1 xs) of
    [] -> Leaf -- should not happen
    x:xs -> x

adjustTopRight :: Ord a => a -> Heap a -> Heap a -> Heap a
adjustTopRight x' l Leaf = Node x' l Leaf
adjustTopRight x' l r@(Node y _ _) | y <= x' = Node y l (adjustTop x' r)
                                   | otherwise = Node x' l r

extractMin' :: Ord a => Heap a -> Maybe (a,Heap a)
extractMin' Leaf = Nothing
extractMin' (Node x l r) =
  case extractMin' l of
    Nothing -> Just (x, r)
    Just (x',l') ->
      Just (x,adjustTopRight x' l' r) 

e = fromList "alligator"

extractAll :: Ord a => Heap a -> [a]
extractAll h =
  case extractMin' h of
    Nothing -> []
    Just (x,h') -> x : extractAll h'

{-
extractAll' :: Ord a => Heap a -> [a]
extractAll' Leaf = []
extractAll' (Node x l r) =
  case extractMin' l of
    Nothing -> x : extractAll' r
    Just (x',l') -> x : 
-}

heapSort :: Ord a => [a] -> [a]
heapSort = extractAll . fromList 
