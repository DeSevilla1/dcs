;; -*- lexical-binding: t; -*-

(require 'dcs-customization)
(require 'dcs-debug)

(defun dcs-mode-clear-highlighting()
  "Remove all faces from the current buffer"
  (with-silent-modifications
    (set-text-properties (point-min) (point-max) nil)))

(defun dcs-mode-categorize (startpos endpos category)
  "Apply the appropriate face for the given category to the text from startpos to endpos inclusive."
  (let ((ep (if (>= endpos (point-max)) (point-max) (+ endpos 1))))
    (with-silent-modifications
      (put-text-property startpos ep 'category category))))

(defun dcs-mode-set-stickiness(cat frontSticky rearSticky)
  "Set the text property on the given category symbol to control front- and rear-stickiness."
  (put cat 'front-sticky frontSticky)
  (put cat 'rear-nonsticky (not rearSticky)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; symbols for the category text property, which we will use for
;; conveniently setting faces, keymaps, and such for various
;; highlighted dcs expressions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; comments (several kinds)
;;
;; set faces and stickiness
(mapcar (lambda (sym)
  (put sym 'face (list :foreground dcs-mode-comment-color)))
        (list 'dcs-mode-comment-open-right
              'dcs-mode-comment-open-left
              'dcs-mode-comment-closed-start-delim
              'dcs-mode-comment-closed-end-delim
              'dcs-mode-comment-closed))
              
(dcs-mode-set-stickiness 'dcs-mode-comment-open-right nil t)
(dcs-mode-set-stickiness 'dcs-mode-comment-open-left t nil)
(dcs-mode-set-stickiness 'dcs-mode-comment-closed-start-delim nil t)
(dcs-mode-set-stickiness 'dcs-mode-comment-closed-end-delim t nil)
(dcs-mode-set-stickiness 'dcs-mode-comment-closed t t)

(defun dcs-mode-log-highlight(knd startpos endpos)
  "Print a log message on highlighting in debug mode."
  (dcs-mode-debug-message
   "highlight %s %d %d in %s"  knd startpos endpos (buffer-name)))


(defun dcs-mode-highlight-comment(knd startpos endpos)
  "Highlight a comment identified by the backend"

;  (dcs-mode-log-highlight knd startpos endpos)

  (let ((commentCat (concat "dcs-mode-comment" "-" knd)))
    (dcs-mode-categorize startpos endpos (intern commentCat))
    (when (equal knd "closed")
      ; need to set the stickiness for the delimiters
      (dcs-mode-categorize startpos (+ startpos 1) (intern (concat commentCat "-start-delim")))
      (dcs-mode-categorize (- endpos 1) endpos (intern (concat commentCat "-end-delim"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; keywords
;;
;; set face and stickiness
(put 'dcs-mode-keyword 'face (list :foreground dcs-mode-keyword-color))
(dcs-mode-set-stickiness 'dcs-mode-keyword nil nil)

(defun dcs-mode-highlight-keyword(startpos endpos)
  "Highlight a keyword identified by the backend"

;  (dcs-mode-log-highlight "keyword" startpos endpos)

  (dcs-mode-categorize startpos endpos 'dcs-mode-keyword))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; errors
;;
;; set face and stickiness
(put 'dcs-mode-error 'face (list :foreground dcs-mode-error-color))
(dcs-mode-set-stickiness 'dcs-mode-error nil nil)

(defun dcs-mode-highlight-error(startpos endpos)
  "Highlight an error identified by the backend"

;  (dcs-mode-log-highlight "error" startpos endpos)
  (dcs-mode-categorize startpos endpos 'dcs-mode-error))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; error focus
;;
;; set face and stickiness
(put 'dcs-mode-error-focus 'face (list :box t :foreground dcs-mode-error-color))
(dcs-mode-set-stickiness 'dcs-mode-error-focus nil nil)

(defvar-local dcs-mode-current-error-focus nil
  "The extent of the current focus, with its previous category")

(defun dcs-mode-clear-error-focus()
  "Clear the error focus if there is one."
  (when dcs-mode-current-error-focus
    (dcs-clear-properties dcs-mode-current-error-focus)
    ; restore just regular error highlighting for the error that was in focus
    (dcs-mode-highlight-error (car dcs-mode-current-error-focus) (cadr dcs-mode-current-error-focus))
    (setq dcs-mode-current-error-focus nil)
    ))

(defun dcs-mode-set-error-focus(startpos endpos)
  (dcs-mode-clear-error-focus)

  ; save the new focus
  (setq dcs-mode-current-error-focus (list startpos endpos))
  
  ; highlight the extent
  (dcs-mode-categorize startpos endpos 'dcs-mode-error-focus)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; navigation focus
;;
;; probably should abstract some of this out as it is just a duplicate
;; of the code above for error focus
;;
;; set face and stickiness
(put 'dcs-mode-navigation-focus 'face (list :slant 'italic :foreground dcs-mode-navigation-color))
(dcs-mode-set-stickiness 'dcs-mode-navigation-focus nil nil)

(defvar-local dcs-mode-current-navigation-focus nil
  "The extent of the current focus")

(make-variable-buffer-local
 (defvar dcs-mode-new-navigation-focus
   nil
   "Variable used during callback handling for setting the navigation focus"))

(defun dcs-mode-clear-navigation-focus-no-continuation()
  "Clear the navigation focus if there is one, with no callback for the backend to issue."
  (interactive)
  (dcs-mode-clear-navigation-focus "(dcs-mode-noop)"))

(defun dcs-mode-clear-navigation-focus-no-rehighlighting()
  "Clear the navigation focus if there is one, without rehighlighting."
  (interactive)
  (when dcs-mode-current-navigation-focus
    (dcs-clear-properties dcs-mode-current-navigation-focus))
   (setq dcs-mode-current-navigation-focus nil))


(defun dcs-mode-clear-navigation-focus(callback)
  "Clear the navigation focus if there is one, requesting the backend to issue the callback, given as a string."
  (interactive)
  (dcs-mode-clear-navigation-focus-no-rehighlighting)
  (dcs-rehighlight callback))

; this function will reorder if startpos > endpos
(defun dcs-mode-set-navigation-focus(startpos endpos)
;  (dcs-mode-debug-message
;     (concat "dcs-mode-set-navigation-focus " (number-to-string startpos) " " (number-to-string endpos)))
  (if (> startpos endpos)
    (dcs-mode-set-navigation-focus endpos startpos)
    (progn
       (setq dcs-mode-new-navigation-focus (list startpos endpos))
       (dcs-mode-clear-navigation-focus "(dcs-finish-set-navigation-focus)") ; callback that rehighlight should issue
    )))

(defun dcs-finish-set-navigation-focus()
  "Will be issued by backend to complete processing after rehighlighting is done "
   ; save the new focus 
  (dcs-mode-debug-message
   (concat "entering dcs-finish-set-navigation-focus with buffer " (buffer-name)))
    (dcs-mode-highlight-errors) ; put error highlighting back in place
    (setq dcs-mode-current-navigation-focus dcs-mode-new-navigation-focus)
    (let ((startpos (nth 0 dcs-mode-current-navigation-focus))
          (endpos (nth 1 dcs-mode-current-navigation-focus)))
      ; highlight the extent
      (dcs-mode-categorize startpos endpos 'dcs-mode-navigation-focus)
))

(defun dcs-mode-point-in-navigation-focusp()
  "Return t/nil depending on whether or not point is in the extent under navigation focus."
  (interactive)
  (when dcs-mode-current-navigation-focus
    (let ((sp (nth 0 dcs-mode-current-navigation-focus))
          (ep (nth 1 dcs-mode-current-navigation-focus)))
      (and (>= (point) sp) (<= (point) ep)))))

(defun dcs-mode-extent-of-navigation-focus()
  "Return a pair of starting and ending position for the current navigation focus"
  (interactive)
  (when dcs-mode-current-navigation-focus
    (cons (nth 0 dcs-mode-current-navigation-focus) (nth 1 dcs-mode-current-navigation-focus))))

(defun dcs-rehighlight(callback)
  "Request highlighting info for the statement containing point"
  (interactive)
  (dcs-mode-send (concat "rehighlight " (number-to-string (point)) " " callback)))

(defun dcs-clear-properties(l)
  "Clear all text properties from the extent specified by the given list, if non-nil"
    (when l
      (let ((s (nth 0 l))
            (e (nth 1 l)))
        (with-silent-modifications
          (set-text-properties s (+ e 1) nil)))))

(provide 'dcs-highlight)


